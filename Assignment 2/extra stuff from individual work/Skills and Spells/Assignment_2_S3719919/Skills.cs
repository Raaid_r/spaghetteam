﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2_S3719919
{
    class Skills
    {
        //Declares variables
        private enum race { Human, Elf, Dwarf, Halfling }; //this will be removed and declared somewhere else
        race ct;
        int skillNumber1;
        int skillNumber2;
        string firstSkill;
        string secondSkill;

        //Picks a set of skills depending on your race
        public void skillPicker()
        {

            Random number = new Random();

            switch (ct)
            {
                case race.Human:
                    skillNumber1 = number.Next(0, 1);
                    skillNumber2 = number.Next(0, 1);
                    break;

                case race.Dwarf:
                    skillNumber1 = number.Next(2, 3);
                    skillNumber2 = number.Next(2, 3);
                    break;

                case race.Elf:
                    skillNumber1 = number.Next(4, 5);
                    skillNumber2 = number.Next(4, 5);
                    break;

                case race.Halfling:
                    skillNumber1 = number.Next(6, 7);
                    skillNumber2 = number.Next(6, 7);
                    break;
            }

            switch (skillNumber1)
            {
                //Cases 0-1 are for humans
                case 0:
                    firstSkill = "Nature";
                    break;
                case 1:
                    firstSkill = "Animal Handling";
                    break;
                //Cases 2-3 are for Dwarfs
                case 2:
                    firstSkill = "History";
                    break;
                case 3:
                    firstSkill = "Intimidation";
                    break;
            //Cases 4-5 are for Elves
                case 4:
                    firstSkill = "Perception";
                    break;
                case 5:
                    firstSkill = "Arcane";
                    break;
            //Cases 6-7 are for Halflings
                case 6:
                    firstSkill = "Acrobats";
                    break;
                case 7:
                    firstSkill = "Deception";
                    break;
            }

            switch (skillNumber1)
            {
                //Cases 0-1 are for humans
                case 0:
                    secondSkill = "Survival";
                    break;
                case 1:
                    secondSkill = "Insight";
                    break;
                //Cases 2-3 are for Dwarfs
                case 2:
                    secondSkill = "Athletics";
                    break;
                case 3:
                    secondSkill = "Religion";
                    break;
                //Cases 4-5 are for Elves
                case 4:
                    secondSkill = "Animal Handling";
                    break;
                case 5:
                    secondSkill = "Nature";
                    break;
                //Cases 6-7 are for Halflings
                case 6:
                    secondSkill = "Stealth";
                    break;
                case 7:
                    firstSkill = "Persuasion";
                    break;
            }
            Console.WriteLine("You're race is " + ct + "\n");
            Console.WriteLine("You're skills are " + firstSkill + " and " + secondSkill);
            Console.Read();
        }
    }
}

/*      Current Algorithm
 *      Spells:
 *      check character INT level
 *      give spells depending on INT level
 *      display spells
 *      Skills:
 *      check character race
 *      give skills depending on race
 *      display skills 
 */




/*      New Algorithm?
 *      Display list of skills/spells
 *      choose skill or random
 *      for choose skill - let user choose from list
 *      for random - use current rng method or basic rng from whole list?
 *      display spells/skills
 */