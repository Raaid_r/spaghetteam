﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2_S3719919
{
    class DnDSkills
    {
        string[] skill = new string[2];
        string randomSkill;
        string getSkill; //Remove this
        string firstSkill; //Remove this
        string secondSkill; //Remove this
        string[] skillName = new string[] { "", "Acrobatics", "Arcana", "Athletics", "Bluff", "Diplomacy", "Dungeoneering", "Endurance", "Heal", "History", "Insight", "Intimidate", "Nature", "Perception", "Religion", "Stealth", "Streetwise", "Theivery" };

        public void skillGet()
        {
            ListSkills();

            GetSkill(); //Remove this
            firstSkill = getSkill; //Remove this

            //Player.AddSkill(GetSkill());


            GetSkill(); //Remove this
            secondSkill = getSkill; //Remove this


            //Player.AddSkill(GetSkill());

            DisplaySkills();
 

            void ListSkills()
            {
                Console.WriteLine("Please choose a spell from the list below:");
                for (int i = 1; i < skillName.Length; i++)
                {
                    Console.WriteLine(i + ". " + skillName[i]);
                }

                Console.WriteLine("");
                Console.WriteLine("0 - RANDOM");
                Console.Write("");
                Console.Write("");
            }

            void DisplaySkills()
            {
                Console.WriteLine("");
                Console.WriteLine("Your skills are " + firstSkill + " and " + secondSkill);
                Console.Read();

            }

            string GetSkill()
            {

                for (int i = 0; i < 1; i++)
                {
                    int choice;
                    Console.WriteLine("");
                    Console.WriteLine("Choose a skill from 0 - 17: ");
                    choice = Int32.Parse(Console.ReadLine());

                    if (choice > 17 || choice < 0)
                    {
                        Console.WriteLine("Error");
                        i--;
                    }
                    else
                    {

                        if (choice == 0)
                        {
                            RandomSkill();

                            skillName[i] = randomSkill;
                            skillName[choice] = skillName[i];
                        }



                        if (skillName[1].Equals(skillName[2]))
                        {
                            Console.WriteLine("You have two of the same skills! Please choose another");
                            i--;
                        }



                        Console.WriteLine(skillName[choice]);
                        getSkill = skillName[choice];
                    }
                }
                return (getSkill);
            }

            string RandomSkill()
            {
                Random number = new Random();
                int randomNumber = number.Next(1, 17);
                switch (randomNumber)
                {
                    case 1:
                        randomSkill = "Acrobatics";
                        break;
                    case 2:
                        randomSkill = "Arcana";
                        break;
                    case 3:
                        randomSkill = "Athletics";
                        break;
                    case 4:
                        randomSkill = "Bluff";
                        break;
                    case 5:
                        randomSkill = "Diplomacy";
                        break;
                    case 6:
                        randomSkill = "Dungeoneering";
                        break;
                    case 7:
                        randomSkill = "Endurance";
                        break;
                    case 8:
                        randomSkill = "Heal";
                        break;
                    case 9:
                        randomSkill = "History";
                        break;
                    case 10:
                        randomSkill = "Insight";
                        break;
                    case 11:
                        randomSkill = "Intimidate";
                        break;
                    case 12:
                        randomSkill = "Nature";
                        break;
                    case 13:
                        randomSkill = "Perception";
                        break;
                    case 14:
                        randomSkill = "Religion";
                        break;
                    case 15:
                        randomSkill = "Stealth";
                        break;
                    case 16:
                        randomSkill = "Streetwise";
                        break;
                    case 17:
                        randomSkill = "Thievery";
                        break;
                }

                return randomSkill;
            }


        }
    }
}



