﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2_S3719919
{
    class Spells
    {
        //Declaring variables
        int inte; //This will be removed when programs are merged
        int spellNumber1 = 0;
        int spellNumber2 = 0;
        string firstSpell;
        string secondSpell;



        public void spellPicker()
        {
            //Declaring variables - these two lines will be removed 
            Random rng = new Random();
            inte = rng.Next(6, 20);


            Random random = new Random();

            //Determines which set of skills will be picked from
            if (inte < 8)
            {
                spellNumber1 = 11;
                spellNumber2 = 11;
            }
            if (inte >= 8 && inte < 10)
            {
                spellNumber1 = random.Next(0, 2);
                spellNumber2 = random.Next(0, 2);
            }
            if (inte >= 10 && inte < 12)
            {
                spellNumber1 = random.Next(3, 4);
                spellNumber2 = random.Next(3, 4);
            }
            if (inte >= 14 && inte < 16)
            {
                spellNumber1 = random.Next(5, 6);
                spellNumber2 = random.Next(5, 6);
            }
            if (inte >= 16 && inte < 18)
            {
                spellNumber1 = random.Next(7, 8);
                spellNumber2 = random.Next(7, 8);
            }
            if (inte >= 18 && inte <= 20)
            {
                spellNumber1 = random.Next(9, 10);
                spellNumber2 = random.Next(9, 10);
            }

            //Picks first spell
            switch (spellNumber1)
            {
                case 0:
                    firstSpell = "Acid Splash";
                    break;
                case 1:
                    firstSpell = "Blade Ward";
                    break;
                case 2:
                    firstSpell = "Booming Blade";
                    break;
                case 3:
                    firstSpell = "Alarm";
                    break;
                case 4:
                    firstSpell = "Cause Fear";
                    break;
                case 5:
                    firstSpell = "Earthbind";
                    break;
                case 6:
                    firstSpell = "Gust of Wind";
                    break;
                case 7:
                    firstSpell = "Animate Dead";
                    break;
                case 8:
                    firstSpell = "Bestow Curse";
                    break;
                case 9:
                    firstSpell = "Foresight";
                    break;
                case 10:
                    firstSpell = "Power Word Kill";
                    break;
                case 11:
                    firstSpell = "None";
                    break;
            }

            //Picks second spell
            switch (spellNumber2)
            {
                case 0:
                    secondSpell = "Chill Touch";
                    break;
                case 1:
                    secondSpell = "Control Flames";       
                    break;
                case 2:
                    secondSpell = "Dancing Lights";
                    break;
                case 3:
                    secondSpell = "Detect Magic";
                    break;
                case 4:
                    secondSpell = "Charm Person";
                    break;
                case 5:
                    secondSpell = "Flaming Sphere";
                    break;
                case 6:
                    secondSpell = "Mind Spike";
                    break;
                case 7:
                    secondSpell = "Phantom Steed";
                    break;
                case 8:
                    secondSpell = "Creation";
                    break;
                case 9:
                    secondSpell = "Gate";
                    break;
                case 10:
                    secondSpell = "Time Stop";
                    break;
                case 11:
                    secondSpell = "None";
                    break;
            }

            //WriteLines out skills
            if (inte > 8)
            {
                Console.WriteLine("You're INT is " + inte + "\n");
                Console.WriteLine("You're spells are " + firstSpell + " and " + secondSpell);
                Console.Read();
            }
            else //If you're INT is too low, no skills for you
            {
                Console.WriteLine("You're intelligence is too low to cast any spells!");
                Console.Read();
            }



        }


    }
}
