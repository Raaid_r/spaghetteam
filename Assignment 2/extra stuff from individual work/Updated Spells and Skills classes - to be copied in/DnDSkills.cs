﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventuresOfSpaghets
{
    class DnDSkills
    {
        string randomSkill;
        string getSkill;
        string[] SkillName = new string[] { "", "Acrobatics", "Arcana", "Athletics", "Bluff", "Diplomacy", "Dungeoneering", "Endurance", "Heal", "History", "Insight", "Intimidate", "Nature", "Perception", "Religion", "Stealth", "Streetwise", "Theivery" };


        public void ListSkills()
        {
            //Prints out list for user to pick from
            Console.WriteLine("");
            Console.WriteLine("\n\nPlease choose a Skill from the list below:");
            for (int i = 1; i < SkillName.Length; i++)
            {
                Console.WriteLine(i + ". " + SkillName[i]);
            }

            Console.WriteLine("");
            Console.WriteLine("ELSE - RANDOM");
            Console.Write("");
            Console.Write("");
        }

        public string GetSkill()
        {
            //Starts main loop for initial spell to allow user to pick a spell
            for (int i = 0; i < 1; i++)
            {
                int choice;
                Console.WriteLine("");
                Console.WriteLine("\nChoose a Skill from 0 - 17: ");

                //checks user input to allow user to choose a spell
                i = Int32.TryParse(Console.ReadLine(), out choice) ? i : i--;
                if (choice > 0 && choice <= 17)
                {
                    getSkill = SkillName[choice];
                    Console.WriteLine("\nYou have chosen the skill " + SkillName[choice]);
                    Console.ReadLine();
                }
                //tryparse outputs choice as 0, hence why it says ELSE - random
                //since if you put any input that isnt 1 - 42, you get 0, thus random
                else if (choice == 0)
                {                
                    Random number = new Random();
                    int random = number.Next(1, 17);
                    getSkill = SkillName[random];
                    Console.WriteLine("\nYou have been assigned a random skill\n");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("\nError\n");
                    Console.ReadLine();
                }
            }
            return getSkill;
        }


    }
}