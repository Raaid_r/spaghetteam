﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventuresOfSpaghets
{
    class Spells
    {
        string randomSpell;
        string getSpell;
        string[] SpellName = new string[] { "", "Acid Splash", "Blade Ward", "Booming Blade", "Chill Touch", "Control Flames", "Create Bonfire", "Dancing Lights", "Druidcraft", "Eldritch Blast", "Fire Bolt", "Friends", "Frostbite", "Green-flame Blade", "Guidance", "Gust", "Hand of Radiance", "Infestation", "Light", "Lightning Lure", "Mage Hand", "Magic Stone", "Mending", "Message", "Minor Illusion", "Mold Earth", "Poison Spray", "Prestidigitation", "Primal Savagery", "Produce Flame", "Ray of Frost", "Resistance", "Sacred Flame", "Shape Water", "Shocking Grasp", "Spare the Dying", "Sword Burst", "Thaumaturgy", "Thorn Whip", "Thunderclap", "True Strike", "Vicious Mockery", "Virtue" };

        public void ListSpells()
        {

            Console.WriteLine("\n\nPlease choose a Spell from the list below:");
            for (int i = 1; i < SpellName.Length; i++)
            {
                Console.WriteLine(i + ". " + SpellName[i]);
            }

            Console.WriteLine("");
            Console.WriteLine("ELSE - RANDOM");
            Console.Write("");
            Console.Write("");

        }

        public string GetSpell()
        {
            //Code for this is almost identical to the skills page
            //Only difference is a lot more spells
            //And random spell is done more simply
            {
                //Starts main loop for initial spell
                for (int i = 0; i < 1; i++)
                {
                    int choice;
                    Console.WriteLine("");
                    Console.WriteLine("\nChoose a Skill from 1 - 42: ");

                    i = Int32.TryParse(Console.ReadLine(), out choice) ? i : i--;
                    if (choice > 0 && choice <= 42)
                    {
                        getSpell = SpellName[choice];
                        Console.WriteLine("\nYou have chosen the spell " + SpellName[choice]);
                        Console.ReadLine();
                    }
                    else if (choice == 0)
                    {
                        Random number = new Random();
                        int random = number.Next(1, 17);
                        getSpell = SpellName[random];
                        Console.WriteLine("\nYou have been assigned a random spell\n");
                        Console.ReadLine();
                    }
                    else
                    {
                        Console.WriteLine("\nError\n");
                        Console.ReadLine();
                    }
                }
                return getSpell;
            }
        }
    }
}

