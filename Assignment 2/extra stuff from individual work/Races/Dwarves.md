DWARVES
---------------------------------------------------
Male Names:
  Thorin
  Ulfgar
  Dain
  Darrak
  Ragnar
  Brottor
Female Names:
  Finellen
  Amber
  Ilde
  Helja
  Sannl
  Hlin
Clan Names:
  Battlehammer
  Fineforge
  Rumnaheim
  Stoneskin
  Frostbeard
  Dankil

General:
  +2 Constitution
  Speed:
    25 - not reduced by heavy armour
----------------------will take out some of these traits (too many)-----
  Traits:
    Darkvision:
      can see dim light as bright light ~20m
      see darkness as greyscaled dim light ~20m
    Dwarven Resilience:
      Increase saving throws against poison
      Resistance to poison damage
    <!-- Dwarven Combat Training:
      proficient with:
        battleaxe
        handaxe
        lighthammer
        warhammer
    Tool proficiency:
      proficiency in one:
        Smith's tools
        brewers supplies
        masons tools -->
    Ability score increase:
      Strength +2
    Dwarven toughness:
      +1 max HP, +1 for each level
