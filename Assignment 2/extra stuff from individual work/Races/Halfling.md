HALFLING
---------------------------------------------------
Male Names:
  Alton, Ander, Cade, Corrin, Eldon, Errich, Finnan, Garret, Lindal, Lyle, Merric, Milo, Osborn, Perrin, Reed, Roscoe, Wellby
Female Names:
  Andry, Bree, Callie, Cora, Euphemia, Jillian, Kithri, Lavinia, Lidda, Merla, Nedda, Paela, Portia, Seraphina, Shaena, Trym, Vani, Verna
Family Names:
  Brushgather, Goodbarrel, Greenbottle, High-hill, Hilltopple, Leagallow, Tealeaf, Thorngage, Tosscobble, Underbough

General
  Dex +2
  Speed 25
  Lucky. When you roll a 1 on an attack roll, ability check, or saving throw, you can reroll the die and must use the new roll.
  Brave. You have advantage on saving throws against being frightened.
  Halfling Nimbleness. You can move through the space of any creature that is of a size larger than yours.
Lightfoot
    Charisma +1
    Naturally Stealthy. You can attempt to hide even when you are obscured only by a creature that is at least one size larger than you.
Stout
    Constitution +1
    Stout Resilience. You have advantage on saving throws against poison, and you have resistance against poison damage.
