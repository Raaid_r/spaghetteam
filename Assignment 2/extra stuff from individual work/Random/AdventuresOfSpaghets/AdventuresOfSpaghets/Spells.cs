﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventuresOfSpaghets
{
    class Spells
    {
        string randomSpell;
        string getSpell;
        string[] spellName = new string[] { "", "Acid Splash", "Blade Ward", "Booming Blade", "Chill Touch", "Control Flames", "Create Bonfire", "Dancing Lights", "Druidcraft", "Eldritch Blast", "Fire Bolt", "Friends", "Frostbite", "Green-flame Blade", "Guidance", "Gust", "Hand of Radiance", "Infestation", "Light", "Lightning Lure", "Mage Hand", "Magic Stone", "Mending", "Message", "Minor Illusion", "Mold Earth", "Poison Spray", "Prestidigitation", "Primal Savagery", "Produce Flame", "Ray of Frost", "Resistance", "Sacred Flame", "Shape Water", "Shocking Grasp", "Spare the Dying", "Sword Burst", "Thaumaturgy", "Thorn Whip", "Thunderclap", "True Strike", "Vicious Mockery", "Virtue" };

        public void ListSpells()
        {

            Console.WriteLine("\n\nPlease choose a Spell from the list below:");
            for (int i = 1; i < spellName.Length; i++)
            {
                Console.WriteLine(i + ". " + spellName[i]);
            }

            Console.WriteLine("");
            Console.WriteLine("ELSE - RANDOM");
            Console.Write("");
            Console.Write("");

        }

        public string GetSpell()
        {
            //Code for this is almost identical to the skills page
            //Only difference is a lot more spells
            //And random spell is done more simply
            for (int i = 0; i < 1; i++)
            {
                int choice;
                Console.WriteLine("");
                Console.WriteLine("\n\nChoose a Spell number from 1 - 42: ");
                choice = Int32.TryParse(Console.ReadLine(), out choice)?i:i--;

                if (choice > 42 || choice < 0)
                {
                    Console.WriteLine("\nError\n");
                    i--;
                }
                else
                {

                    if (choice == 0)
                    {
                        Console.WriteLine("\nYou have been assigned a random spell\n");
                        Random number = new Random();
                        choice = number.Next(1, 42);
                    }



                    if (spellName[1].Equals(spellName[2]))
                    {
                        Console.WriteLine("\nYou have two of the same spells! Please choose another\n");
                        i--;
                    }



                    Console.WriteLine(spellName[choice]);
                    getSpell = spellName[choice];
                }
            }
            return (getSpell);
        }









    }
}

