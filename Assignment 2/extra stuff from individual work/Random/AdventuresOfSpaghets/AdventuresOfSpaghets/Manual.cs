﻿using System;
namespace AdventuresOfSpaghets
{
    public class Manual  /*Create Character Attributes*/
    {

        public enum Selection { Player, Race, Class, Alignment, Ability_Scores, Character_Name };
        public enum Race { Dwarf, Elf, Halfling, Human }; //25, 30, 25, 30
        public int speed;
        public enum Class { Cleric, Fighter, Rogue, Wizard };
        public enum Alignment
        {
            Lawful_good, Neutral_good, Chaotic_good,
            Lawful_neutral, Neutral, Chaotic_neutral, Lawful_evil, Neutral_evil, Chaotic_evil
        };
        public enum Ability { Strength, Dexterity, Constitution, Intelligence, Wisdom, Charisma };



        //Human
        public enum Human { Galishite, Chondathan, Damaran, Rashemi, Shou, Turami };

        //Galishite
        public enum Galishite { GalishiteMale, GalishiteFemale, GalishiteSurnames };
        public enum GalishiteMale { Aseir, Bardeid, Haseid, Khemed, Mehmen, Sudeiman, Zasheir };
        public enum GalishiteFemale { Atala, Ceidil, Hama, Jasmal, Meilil, Seipora, Yasheira, Zasheida };
        public enum GalishiteSurnames { Basha, Dumein, Jassan, Khalid, Mostana, Pashar, Rein };

        //Chondathan
        public enum Chondathan { ChondathanMale, ChondathanFemale, ChondathanSurnames };
        public enum ChondathanMale { Darvin, Dorn, Evendur, Gorstag, Grim, Helm, Malark, Morn, Randal, Stedd };
        public enum ChondathanFemale { Arveene, Esvele, Jhessail, Kerri, Lureene, Miri, Rowan, Shandri, Tessele };
        public enum ChondathanSurnames { Amblecrown, Buckman, Dundragon, Evenwood, Greycastle, Tallstag };


        //Damaran
        public enum Damaran { DamaranMale, DamaranFemale, DamaranSurnames };
        public enum DamaranMale { Bor, Fodel, Glar, Grigor, Igan, Ivor, Kosef, Mival, Orel, Pavel, Sergor };
        public enum DamaranFemale { Alethra, Kara, Katernin, Mara, Natali, Olma, Tana, Zora };
        public enum DamaranSurnames { Bersk, Chernin, Dotsk, Kulenov, Marsk, Nemetsk, Shemov, Starag };


        //Rashemi
        public enum Rashemi { RashemiMale, RashemiFemale, RashemiSurnames };
        public enum RashemiMale { Borivik, Faurgar, Jandar, Kanithar, Madislak, Ralmevik, Shaumar, Vladislak };
        public enum RashemiFemale { Fyevarra, Hulmarra, Immith, Imzel, Navarra, Shevarra, Tammith, Yuldra };
        public enum RashemiSurnames { Chergoba, Dyernina, Iltazyara, Murnyethara, Stayanoga, Ulmokina };


        //Shou
        public enum Shou { ShouMale, ShouFemale, ShouSurname };
        public enum ShouMale { An, Chen, Chi, Fai, Jiang, Jun, Lian, Long, Meng, On, Shan, Shui, Wen };
        public enum ShouFemale { Bai, Chao, Jia, Lei, Mei, Qiao, Shui, Tai };
        public enum ShouSurname { Chien, Huang, Kao, Kung, Lao, Ling, Mei, Pin, Shin, Sum, Tan, Wan };


        //Turami
        public enum Turami { uramiMale, TuramiFemale, TuramiSurnames };
        public enum TuramiMale { Anton, Diero, Marcon, Pieron, Rimardo, Romero, Salazar, Umbero };
        public enum TuramiFemale { Balama, Dona, Faila, Jalana, Luisa, Marta, Quara, Selise, Vonda };
        public enum TuramiSurnames { Agosto, Astorio, Calabra, Domine, Falone, Marivaldi, Pisacar, Ramondo };





       
      


        public String CreateRace(String inputRace)
        {
            switch (inputRace)
            {
                case "0":
                    inputRace = Enum.GetName(typeof(Race), 0);
                    Console.WriteLine("Speed is 25");
                    break;

                case "1":
                    Console.WriteLine("Speed is 30");
                    inputRace = Enum.GetName(typeof(Race), 1);
                    break;

                case "2":
                    Console.WriteLine("Speed is 25");
                    inputRace = Enum.GetName(typeof(Race), 2);
                    break;
                case "3":
                    Console.WriteLine("Speed is 30");
                    inputRace = Enum.GetName(typeof(Race), 3);
                    break;
                default:
                    Console.WriteLine("Wrong input, Please input correct" + " +2 Constitution\n 25 base speed - not effected by heavy armour\n\n Dwarven Toughness:\n +2 Strength\n +1 max HP, +1 for each level\n\n Dark Vision:\n ~20m Dim Light seen as Bright Light\n ~20m Darkness seen as greyscaled Dim Light");
                    inputRace = "";
                    break;
            }
            return inputRace;
        }





        public String CreateClass(String inputClass)
        {
                switch (inputClass)
                {
                    case "0":
                        inputClass = Enum.GetName(typeof(Class), 0);
                        break;

                    case "1":
                        inputClass = Enum.GetName(typeof(Class), 1);
                        break;

                    case "2":
                        inputClass = Enum.GetName(typeof(Class), 2);
                        break;

                    case "3":
                        inputClass = Enum.GetName(typeof(Class), 3);
                        break;

                    default:
                        Console.WriteLine("Wrong input, Please input correct");
                        break;
            }
            return inputClass;
        }







        public String CreateAlignment(String inputAlignment)
        {
                switch (inputAlignment)
                {
                    case "0":
                        inputAlignment = Enum.GetName(typeof(Alignment), 0);
                        break;

                    case "1":
                        inputAlignment = Enum.GetName(typeof(Alignment), 1);
                        break;

                    case "2":
                        inputAlignment = Enum.GetName(typeof(Alignment), 2);
                        break;

                    case "3":
                        inputAlignment = Enum.GetName(typeof(Alignment), 3);
                        break;

                    case "4":
                        inputAlignment = Enum.GetName(typeof(Alignment), 4);
                        break;

                    case "5":
                        inputAlignment = Enum.GetName(typeof(Alignment), 5);
                        break;

                    case "6":
                        inputAlignment = Enum.GetName(typeof(Alignment), 6);
                        break;

                    case "7":
                        inputAlignment = Enum.GetName(typeof(Alignment), 7);
                        break;

                    case "8":
                        inputAlignment = Enum.GetName(typeof(Alignment), 8);
                        break;

                    default:
                        Console.WriteLine("Wrong input, Please input correct");
                        break;
            }
            return inputAlignment;
        }



        public String CreateAbility(String inputAbility)
        {
            switch (inputAbility)
            {
                case "0":
                    inputAbility = Enum.GetName(typeof(Ability), 0);
                    break;

                case "1":
                    inputAbility = Enum.GetName(typeof(Ability), 1);
                    break;

                case "2":
                    inputAbility = Enum.GetName(typeof(Ability), 2);
                    break;

                case "3":
                    inputAbility = Enum.GetName(typeof(Ability), 3);
                    break;

                case "4":
                    inputAbility = Enum.GetName(typeof(Ability), 4);
                    break;

                case "5":
                    inputAbility = Enum.GetName(typeof(Ability), 5);
                    break;

                default:
                    Console.WriteLine("Wrong input, Please input correct");
                    break;
            }
            return inputAbility;
        }



            public String SetAbilityScore(String inputAbilityScore)
            {
                    switch (inputAbilityScore)
                    {
                        case "1":
                            inputAbilityScore = "1";
                            break;

                        case "2":
                            inputAbilityScore = "2";
                            break;

                        case "3":
                            inputAbilityScore = "3";
                            break;

                        case "4":
                            inputAbilityScore = "4";
                            break;

                        case "5":
                            inputAbilityScore = "5";
                            break;

                        case "6":
                            inputAbilityScore = "6";
                            break;

                        default:
                            Console.WriteLine("Wrong input, Please input correct");
                            break;
                }
                return inputAbilityScore;
            }
       


      

    }

}