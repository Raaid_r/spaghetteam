﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventuresOfSpaghets
{
    class Random1
    {

        public enum Selection { Race, Class, Alignment, Ability_Scores, Character_Name };
        public enum Race { Dwarf, Elf, Halfling, Human };
        public enum Class { Cleric, Fighter, Rogue, Wizard };
        public enum Alignment { Lawful_good, Neutral_good, Chaotic_good, Lawful_neutral, Neutral, Chaotic_neutral, Lawful_evil, Neutral_evil, Chaotic_evil };
        public enum AbilityScore { Strength, Dexterity, Constitution, Intelligence, Wisdom, Charisma };

        public String[,] Name = new String[12, 6]
        {
            // dwarves
            { "Thorin", "Ulfgar", "Dain", "Darrak", "Ragnar", "Brottor" },
            { "Finellen", "Amber", "Ilde", "Helja", "Sannl", "Hlin" },
            { "Battlehammer", "Fineforge", "Rumnaheim", "Stoneskin", "Frostbeard", "Dankil" },
            // elves
            { "Aeler", "Erevan", "Ivellios", "Paelias", "Tharivol", "Varis" },
            { "Adrie", "Birel", "Enna", "Keyleth", "Meriele", "Sariel" },
            { "Amakiir (Gemflower)", "Ilphelkiir (Gemblossom)", "Meliamne (Oakenheel)", "Liadon (Silverfrond)", "Siannodel (Moonbrook)", "Xiloscient (Goldpetal)" },
            // halflings
            { "Alton", "Cade", "Eldon", "Garret", "Milo", "Wellby"},
            { "Andry", "Callie", "Lavinia", "Nedda", "Vani", "Shaena"},
            { "Brushgather", "Goodbarrel", "Tosscobble", "Greenbottle", "Hilltopple", "Tealeaf"},
            // humans
            { "Haseid", "Stedd", "Fodel", "Lander", "Ramas", "Shaumar"},
            { "Atala", "Shandri", "Natali", "Kethra", "Tammith", "Jalana"},
            { "Marsk", "Windrivver", "Uuthrakt", "Iltazyara", "Ramondo", "Huang"}
        };


        // garbbing RandomGen
        RandomGen rnd = new RandomGen();
        int raceValue = default(int);

        //creating race
        public String CreateRace()
        {
            raceValue = rnd.RandomCreate(0, 4);
            String inputRace = Enum.GetName(typeof(Race), raceValue);
            return inputRace;
        }

        public int RaceValue()
        {
            return raceValue;
        }

        public int CreateSpeed()
        {
            int speed = default(int);
            switch (raceValue)
            {
                case 0:
                    speed = 25;
                    break;
                case 1:
                    speed = 30;
                    break;
                case 2:
                    speed = 25;
                    break;
                case 3:
                    speed = 30;
                    break;
            }
            return speed;
        }

        //creating class
        public String CreateClass()
        {
            String inputClass = Enum.GetName(typeof(Class), rnd.RandomCreate(0, 4));
            return inputClass;
        }


        // creating alignment
        public String CreateAlignment()
        {
            String inputAlignment = Enum.GetName(typeof(Alignment), rnd.RandomCreate(0, 9));
            return inputAlignment;
        }

        // creating ability scores/stats
        public int CreateAbilityScores()
        {
            int inputAbilityScores = rnd.RandomStats();
            return inputAbilityScores;

        }


        // creating names
        public String CreatefName(int male, int female)
        {
            int randomValue = rnd.RandomCreate(0, 6);
            int randomValue1 = rnd.RandomCreate(male, female);
            String fName = Name[randomValue1, randomValue];
            return fName;
        }

        public String CreatelName(int lastName)
        {
            int randomValue = rnd.RandomCreate(0, 6);
            String lName = Name[lastName, randomValue];
            return lName;
        }


    }
}
