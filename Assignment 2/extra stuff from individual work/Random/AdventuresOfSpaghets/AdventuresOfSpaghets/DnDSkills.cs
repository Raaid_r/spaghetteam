﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventuresOfSpaghets
{
    class DnDSkills
    {
        string randomSkill;
        string getSkill;
        string[] SkillName = new string[] { "", "Acrobatics", "Arcana", "Athletics", "Bluff", "Diplomacy", "Dungeoneering", "Endurance", "Heal", "History", "Insight", "Intimidate", "Nature", "Perception", "Religion", "Stealth", "Streetwise", "Theivery" };


        public void ListSkills()
            {
            Console.WriteLine("");
                Console.WriteLine("\n\nPlease choose a Skill from the list below:");
                for (int i = 1; i < SkillName.Length; i++)
                {
                    Console.WriteLine(i + ". " + SkillName[i]);
                }

                Console.WriteLine("");
                Console.WriteLine("0 - RANDOM");
                Console.Write("");
                Console.Write("");
            }

        public string GetSkill()
            {
                //Starts main loop for two initial spells
                for (int i = 0; i < 1; i++)
                {
                    int choice;
                    Console.WriteLine("");
                    Console.WriteLine("\nChoose a Skill from 0 - 17: ");

                 choice = Int32.Parse(Console.ReadLine());
                
                    //Gives an error if you put in an incorrent number
                    if (choice > 17 || choice < 0)
                    {
                        Console.WriteLine("\nError\n");
                        i--;
                    }
                    else
                    {
                        //Gives random skill
                        if (choice == 0)
                        {
                            RandomSkill();

                            SkillName[i] = randomSkill;
                            SkillName[choice] = SkillName[i];
                        }


                        //No two same starting skills
                        if (SkillName[1].Equals(SkillName[2]))
                        {
                            Console.WriteLine("\nYou have two of the same Skills! Please choose another\n");
                            i--;
                        }



                        Console.WriteLine(SkillName[choice]);
                        getSkill = SkillName[choice];
                    }
                }
                return (getSkill);
            }
        //Code for random skills
        public string RandomSkill()
            {
                Random number = new Random();
                int randomNumber = number.Next(1, 17);
                switch (randomNumber)
                {
                    case 1:
                        randomSkill = "Acrobatics";
                        break;
                    case 2:
                        randomSkill = "Arcana";
                        break;
                    case 3:
                        randomSkill = "Athletics";
                        break;
                    case 4:
                        randomSkill = "Bluff";
                        break;
                    case 5:
                        randomSkill = "Diplomacy";
                        break;
                    case 6:
                        randomSkill = "Dungeoneering";
                        break;
                    case 7:
                        randomSkill = "Endurance";
                        break;
                    case 8:
                        randomSkill = "Heal";
                        break;
                    case 9:
                        randomSkill = "History";
                        break;
                    case 10:
                        randomSkill = "Insight";
                        break;
                    case 11:
                        randomSkill = "Intimidate";
                        break;
                    case 12:
                        randomSkill = "Nature";
                        break;
                    case 13:
                        randomSkill = "Perception";
                        break;
                    case 14:
                        randomSkill = "Religion";
                        break;
                    case 15:
                        randomSkill = "Stealth";
                        break;
                    case 16:
                        randomSkill = "Streetwise";
                        break;
                    case 17:
                        randomSkill = "Thievery";
                        break;
                }

                return randomSkill;
            }


        
    }
}