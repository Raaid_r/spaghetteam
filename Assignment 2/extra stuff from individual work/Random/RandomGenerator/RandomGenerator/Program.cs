﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomGenerator
{
    public enum Race { Dwarf, Elf, Halfling, Human };
    public enum Class { Cleric, Fighter, Rogue, Wizard };
    public enum Alignment { Lawful_good, Neutral_good, Chaotic_good, Lawful_neutral, Neutral, Chaotic_neutral, Lawful_evil, Neutral_evil, Chaotic_evil };
    public enum AbilityScore { Strength, Dexterity, Constitution, Intelligence, Wisdom, Charisma };
    //names
    public enum dwarfMNames { Thorin, Ulfgar, Dain, Darrak, Ragnar, Brottor };
    public enum dwarfFNames { Finellen, Amber, Ilde, Helja, Sannl, Hlin };
    public enum DwarfLNames { Battlehammer, Fineforge, Rumnaheim, Stoneskin, Frostbeard, Dankil };

    class Program
    {


        static void Main(string[] args)
        {

            /*  Things I need to add:
             *      store all values in variables to be used for loading/saving
             *      loop each menu segment instead of skipping to next after invalid entry
             *      Skip speed ~predetermined by race
             *      character name
             *          enums for all the possible names from each race
             *          set amount of possible names (6 each?)
             */
            String input = "";
            Random rnd = new Random();
            String[] savePlayer = {"Player name", "Race", "Class", "Level", "Speed", "Alignment", "Character Name"};
            // Menu for races, I tried not to hardcode too much, not sure if i should keep it this way.
            Console.WriteLine("Choose your Race:\n0. {0}\n1. {1}\n2. {2}\n3. {3}\n \nX - random", Enum.GetName(typeof(Race), 0), Enum.GetName(typeof(Race), 1), Enum.GetName(typeof(Race), 2), Enum.GetName(typeof(Race), 3));
            input = Console.ReadLine();
            int raceRandom = default(int);
            if (input.Equals("x") || input.Equals("X"))
            {
                raceRandom = rnd.Next(0, 3);
                savePlayer[1] = Enum.GetName(typeof(Race), raceRandom);
                Console.WriteLine("\n" + raceRandom + ". " + savePlayer[1] + "\n");
            }
            else
            {
                Console.WriteLine("error, unknown input!!");
            }
            //Menu for classes
            Console.WriteLine("Choose your Class:\n0. {0}\n1. {1}\n2. {2}\n3. {3}\n \nX - random",
                Enum.GetName(typeof(Class), 0),
                Enum.GetName(typeof(Class), 1),
                Enum.GetName(typeof(Class), 2),
                Enum.GetName(typeof(Class), 3));
            input = Console.ReadLine();
            if (input.Equals("x") || input.Equals("X"))
            {
                int classRandom = rnd.Next(0, 3);
                savePlayer[2] = Enum.GetName(typeof(Class), classRandom);
                Console.WriteLine("\n" + classRandom + ". " + savePlayer[2] + "\n");
            }
            else
            {
                Console.WriteLine("error, unknown input!!");
            }
            //Menu for Alignment
            Console.WriteLine("Choose your Alignment:\n0. {0}\n1. {1}\n2. {2}\n3. {3}\n4. {4}\n5. {6}\n7. {7}\n8. {8}\n \nX - random",
                Enum.GetName(typeof(Alignment), 0),
                Enum.GetName(typeof(Alignment), 1),
                Enum.GetName(typeof(Alignment), 2),
                Enum.GetName(typeof(Alignment), 3),
                Enum.GetName(typeof(Alignment), 4),
                Enum.GetName(typeof(Alignment), 5),
                Enum.GetName(typeof(Alignment), 6),
                Enum.GetName(typeof(Alignment), 7),
                Enum.GetName(typeof(Alignment), 8));
            input = Console.ReadLine();
            if (input.Equals("x") || input.Equals("X"))
            {
                int alignmentRandom = rnd.Next(0, 8);
                savePlayer[5] = Enum.GetName(typeof(Alignment), alignmentRandom);
                Console.WriteLine("\n" + alignmentRandom + ". " + savePlayer[5] + "\n");
            }
            else
            {
                Console.WriteLine("error, unknown input!!");
            }

            //Menu for Ability scores  WIP-------------------------
            /* Console.WriteLine("Choose your Ability Scores:\n0. {0}\n1. {1}\n2. {2}\n3. {3}\n4. {4}\n5. {6}\n7. {7}\n8. {8}\n \nx - random",
                Enum.GetName(typeof(Alignment), 0),
                Enum.GetName(typeof(Alignment), 1),
                Enum.GetName(typeof(Alignment), 2),
                Enum.GetName(typeof(Alignment), 3),
                Enum.GetName(typeof(Alignment), 4),
                Enum.GetName(typeof(Alignment), 5),
                Enum.GetName(typeof(Alignment), 6),
                Enum.GetName(typeof(Alignment), 7),
                Enum.GetName(typeof(Alignment), 8)); */
            Console.WriteLine("Choose (roll) your Ability Scores: \n \nX - random");
            input = Console.ReadLine();
            if (input.Equals("x") || input.Equals("X"))
            {
                for (int i = 0; i < Enum.GetNames(typeof(AbilityScore)).Length; i++)
                {
                    int d6_0 = rnd.Next(1, 6);
                    int d6_1 = rnd.Next(1, 6);
                    int d6_2 = rnd.Next(1, 6);
                    int d6_3 = rnd.Next(1, 6);
                    var list = new[] { d6_0, d6_1, d6_2, d6_3 };
                    int min = list.Min();
                    int score = d6_0 + d6_1 + d6_2 + d6_3 - min;
                    Console.WriteLine("{0} = " + score, Enum.GetName(typeof(AbilityScore), i));
                }
            }
            else
            {
                Console.WriteLine("error, unknown input!!");
            }

            Console.WriteLine("\nChoose your name {0}:", savePlayer[1]);

            switch (raceRandom)
            {
                case (int)Race.Dwarf:
                    Console.WriteLine("Dwarf names");
                    break;
                case (int)Race.Elf:
                    Console.WriteLine("Elf names");
                    break;
                case (int)Race.Halfling:
                    Console.WriteLine("Halfling names");
                    break;
                case (int)Race.Human:
                    Console.WriteLine("Human names");
                    break;

            }

            Console.WriteLine("\n end of program, any key to exit.");
            Console.ReadLine();

        }
    }
}
