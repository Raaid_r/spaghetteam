﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menu_test_mac
{
    public enum Selection { Player, Race, Class, Alignment, Ability_Scores, Character_Name };
    public enum Race { Dwarf, Elf, Halfling, Human };
    public enum Class { Cleric, Fighter, Rogue, Wizard };
    public enum Alignment { Lawful_good, Neutral_good, Chaotic_good, Lawful_neutral, Neutral, Chaotic_neutral, Lawful_evil, Neutral_evil, Chaotic_evil };
    public enum AbilityScore { Strength, Dexterity, Constitution, Intelligence, Wisdom, Charisma };

    class Program
    {
        static void Main(string[] args)
        {
            // names
            String[,] Name = new String[12, 6]
            {
                // dwarves
                { "Thorin", "Ulfgar", "Dain", "Darrak", "Ragnar", "Brottor" },
                { "Finellen", "Amber", "Ilde", "Helja", "Sannl", "Hlin" },
                { "Battlehammer", "Fineforge", "Rumnaheim", "Stoneskin", "Frostbeard", "Dankil" },
                // elves
                { "Aeler", "Erevan", "Ivellios", "Paelias", "Tharivol", "Varis" },
                { "Adrie", "Birel", "Enna", "Keyleth", "Meriele", "Sariel" },
                { "Amakiir (Gemflower)", "Ilphelkiir (Gemblossom)", "Meliamne (Oakenheel)", "Liadon (Silverfrond)", "Siannodel (Moonbrook)", "Xiloscient (Goldpetal)" },
                // halflings
                { "Alton", "Cade", "Eldon", "Garret", "Milo", "Wellby"},
                { "Andry", "Callie", "Lavinia", "Nedda", "Vani", "Shaena"},
                { "Brushgather", "Goodbarrel", "Tosscobble", "Greenbottle", "Hilltopple", "Tealeaf"},
                // humans
                { "Haseid", "Stedd", "Fodel", "Lander", "Ramas", "Shaumar"},
                { "Atala", "Shandri", "Natali", "Kethra", "Tammith", "Jalana"},
                { "Marsk", "Windrivver", "Uuthrakt", "Iltazyara", "Ramondo", "Huang"}
            };

            String[] savePlayer = { "Select Player Name", "Select Race", "Speed - Select Race", "Select Class", "Level - Select Class", "Select Alignment", "Select Character Name", "Roll Ability Scores" };

            // hardcoded sorta stuff
            Console.WriteLine("===================== Dungeons and Dragons =====================");
            for (int i = 0; i < Enum.GetNames(typeof(Selection)).Length; i++)
            {
                Console.WriteLine("(" + i + ") " + "{0, -20} {1, 4:N1}", Enum.GetName(typeof(Selection), i), savePlayer[i]);
            }
            Console.ReadKey();


            String input = "";
            String raceSave = default(String);
            int speed = default(int);
            int randomValue = default(int);
            int raceValue = default(int);


            // race menu list ===================================================
            bool loopThrough = true;
            while (loopThrough == true)
            {
                Console.WriteLine("\nChoose your {0}:", Enum.GetName(typeof(Selection), 1));
                for (int i = 0; i < Enum.GetNames(typeof(Race)).Length; i++)
                {
                    Console.WriteLine("(" + i + ") " + Enum.GetName(typeof(Race), i));
                }
                Console.WriteLine("\n\n # - Select\n X - Random");

                // loop for manual or random or wrong input
                while (loopThrough == true)
                {
                    // random selection input
                    input = Console.ReadLine();
                    if (input.Equals("x") || input.Equals("X"))
                    {
                        RandomGen Race = new RandomGen();
                        // using raceValue instead of randomvalue to choose different names for each race later
                        raceValue = Race.RandomCreate(0, 4);
                        raceSave = Enum.GetName(typeof(Race), raceValue);

                        loopThrough = false;
                    }
                    // manual selection input
                    else if (input.Equals("0") ||
                             input.Equals("1") ||
                             input.Equals("2") ||
                             input.Equals("3"))
                    {
                        loopThrough = false;
                    }
                    else
                    {
                        Console.WriteLine("error, unknown input!!");
                        loopThrough = true;
                    }
                }

                // rewrite this code for "a" and "an" before {0} like tonys code
                Console.WriteLine("You are a {0}, would you like to keep this Race? y/n", raceSave);


                // yes / no / wrong input loop ---- could put this into method
                bool loopThrough1 = true;
                while (loopThrough1 == true)
                {
                    input = Console.ReadLine();
                    if (input.Equals("y") || input.Equals("Y"))
                    {

                        loopThrough1 = false;
                        loopThrough = false;
                    }
                    else if (input.Equals("n") || input.Equals("N"))
                    {
                        raceSave = default(String);
                        loopThrough1 = false;
                        loopThrough = true;
                    }
                    else
                    {
                        Console.WriteLine("error, unknown input!!");
                        loopThrough1 = true;
                    }
                }
            }

            // info on the chosen race
            Console.WriteLine("\nHello {0}, your Race traits are:\n", raceSave);
            switch (raceValue)
            {
                case 0:
                    // Dwarf
                    speed = 25;
                    Console.WriteLine(" +2 Constitution\n 25 base speed - not effected by heavy armour\n\n Dwarven Toughness:\n +2 Strength\n +1 max HP, +1 for each level\n\n Dark Vision:\n ~20m Dim Light seen as Bright Light\n ~20m Darkness seen as greyscaled Dim Light");
                    break;
                case 1:
                    // Elf
                    speed = 30;
                    Console.WriteLine(" +2 Dexterity\n 30 base speed\n\n Mask of the Wild:\n +1 Wisdom\nYou can attempt to hide even when you are only lightly obscured by foliage, heavy rain, falling snow, mist, and other natural phenomena.\n\n Dark Vision:\n ~20m Dim Light seen as Bright Light\n ~20m Darkness seen as greyscaled Dim Light");
                    break;
                case 2:
                    // Halfling
                    speed = 25;
                    Console.WriteLine(" +2 Dexterity\n 25 base speed\n\n Lightfoot:\n +1 Charisma\n You can move through the space of any creature that is a larger size than you\n You can attempt to hide even when only obscured by a creature that is at least one size larger than you\n\n Lucky:\n When you\n  attack roll\n  ability check\n  saving throw\n you can re-roll but must use the new roll");
                    break;
                case 3:
                    // Human
                    speed = 30;
                    Console.WriteLine(" +1 Strength\n+1 Dexterity\n+1 Constitution\n+1 Intelligence\n+1 Wisdom\n +1 Charisma\n 30 base speed");
                    break;
            }
            Console.ReadKey();


            // class menu list =====================================================
            String classSave = default(String);
            loopThrough = true;
            while (loopThrough == true)
            {
                Console.WriteLine("\nChoose your {0}:", Enum.GetName(typeof(Selection), 2));
                for (int i = 0; i < Enum.GetNames(typeof(Class)).Length; i++)
                {
                    Console.WriteLine("(" + i + ") " + Enum.GetName(typeof(Class), i));
                }
                Console.WriteLine("\n\n # - Select\n X - Random");

                // loop for manual or random or wrong input
                while (loopThrough == true)
                {
                    // random selection input
                    input = Console.ReadLine();
                    if (input.Equals("x") || input.Equals("X"))
                    {
                        RandomGen Class = new RandomGen();
                        randomValue = Class.RandomCreate(0, 4);
                        classSave = Enum.GetName(typeof(Class), randomValue);

                        loopThrough = false;
                    }
                    // manual selection input
                    else if (input.Equals("0") ||
                             input.Equals("1") ||
                             input.Equals("2") ||
                             input.Equals("3"))
                    {
                        loopThrough = false;
                    }
                    else
                    {
                        Console.WriteLine("error, unknown input!!");
                        loopThrough = true;
                    }
                }

                // rewrite this code for "a" and "an" before {0} like tonys code
                Console.WriteLine("You are a {0}, would you like to keep this Class? y/n", classSave);


                // yes / no / wrong input loop ---- could put this into method
                bool loopThrough1 = true;
                while (loopThrough1 == true)
                {
                    input = Console.ReadLine();
                    if (input.Equals("y") || input.Equals("Y"))
                    {

                        loopThrough1 = false;
                        loopThrough = false;
                    }
                    else if (input.Equals("n") || input.Equals("N"))
                    {
                        raceSave = default(String);
                        loopThrough1 = false;
                        loopThrough = true;
                    }
                    else
                    {
                        Console.WriteLine("error, unknown input!!");
                        loopThrough1 = true;
                    }
                }
            }

            // info on the chosen class
            Console.WriteLine("\nHello {0}, your Class traits are:\n", classSave);
            switch (randomValue)
            {
                case 0:
                    // Cleric
                    Console.WriteLine("A priestly champion who wields divine magic in service of a higher power.\n Hit Die: d8\n Primary Ability: Wisdom\n Prof. in: Wisdom and Charisma");
                    break;
                case 1:
                    // Fighter
                    Console.WriteLine("A master of martial combat, skilled with a variety of weapons and armor.\n Hit Die: d10\n Primary Ability: Strength/Dexterity\n Prof. in: Strength and Constitution");
                    break;
                case 2:
                    // Rogue
                    Console.WriteLine("A scoundrel who uses stealth and trickery to overcome obstacles and enemies.\n Hit Die: d8\n Primary Ability: Dexterity and Intelligence\n Prof. in: Dexterity and Intelligence");
                    break;
                case 3:
                    // Wizard
                    Console.WriteLine("A scholarly magic-user capable of manipulating the structures of reality.\n Hit Die: d6\n Primary Ability: Intelligence and Wisdom\n Prof. in: Intelligence and Wisdom");
                    break;
            }
            Console.ReadKey();

            // class alignment list =====================================================
            String alignmentSave = default(String);
            loopThrough = true;
            while (loopThrough == true)
            {
                Console.WriteLine("\nChoose your {0}:", Enum.GetName(typeof(Selection), 3));
                for (int i = 0; i < Enum.GetNames(typeof(Alignment)).Length; i++)
                {
                    Console.WriteLine("(" + i + ") " + Enum.GetName(typeof(Alignment), i));
                }
                Console.WriteLine("\n\n # - Select\n X - Random");

                // loop for manual or random or wrong input
                while (loopThrough == true)
                {
                    // random selection input
                    input = Console.ReadLine();
                    if (input.Equals("x") || input.Equals("X"))
                    {
                        RandomGen Alignment = new RandomGen();
                        randomValue = Alignment.RandomCreate(0, 9);
                        alignmentSave = Enum.GetName(typeof(Alignment), randomValue);

                        loopThrough = false;
                    }
                    // manual selection input
                    else if (input.Equals("0") ||
                             input.Equals("1") ||
                             input.Equals("2") ||
                             input.Equals("3") ||
                             input.Equals("4") ||
                             input.Equals("5") ||
                             input.Equals("6") ||
                             input.Equals("7") ||
                             input.Equals("8"))
                    {
                        loopThrough = false;
                    }
                    else
                    {
                        Console.WriteLine("error, unknown input!!");
                        loopThrough = true;
                    }
                }

                // rewrite this code for "a" and "an" before {0} like tonys code
                Console.WriteLine("You are {0}, would you like to keep this Alignment? y/n", alignmentSave);


                // yes / no / wrong input loop ---- could put this into method
                bool loopThrough1 = true;
                while (loopThrough1 == true)
                {
                    input = Console.ReadLine();
                    if (input.Equals("y") || input.Equals("Y"))
                    {

                        loopThrough1 = false;
                        loopThrough = false;
                    }
                    else if (input.Equals("n") || input.Equals("N"))
                    {
                        raceSave = default(String);
                        loopThrough1 = false;
                        loopThrough = true;
                    }
                    else
                    {
                        Console.WriteLine("error, unknown input!!");
                        loopThrough1 = true;
                    }
                }
            }

            // info on the chosen alignment
            Console.WriteLine("\n{0}, your Alignment details are:\n", raceSave);
            switch (randomValue)
            {
                case 0:
                    // Lawful Good
                    Console.WriteLine("creatures can be counted on to do the right thing as expected by society.");
                    break;
                case 1:
                    // Neutral Good
                    Console.WriteLine("folk do the best they can to help others according to their needs.");
                    break;
                case 2:
                    // Chaotic Good
                    Console.WriteLine("creatures act as their conscience directs, with little regard for what others expect.");
                    break;
                case 3:
                    // Lawful Neutral
                    Console.WriteLine("individuals act in accordance with law, tradition, or personal codes.");
                    break;
                case 4:
                    // Neutral
                    Console.WriteLine("is the alignment of those who prefer to steer clear of moral questions and don’t take sides, doing what seems best at the time.");
                    break;
                case 5:
                    // Chaotic Neutral
                    Console.WriteLine("creatures follow their whims, holding their personal freedom above all else.");
                    break;
                case 6:
                    // Lawful Evil
                    Console.WriteLine("creatures methodically take what they want, within the limits of a code of tradition, loyalty, or order.");
                    break;
                case 7:
                    // Neutral Evil
                    Console.WriteLine("is the alignment of those who do whatever they can get away with, without compassion or qualms.");
                    break;
                case 8:
                    // Chaotic Evil
                    Console.WriteLine("creatures act with arbitrary violence, spurred by their greed, hatred, or bloodlust.");
                    break;
            }
            Console.ReadKey();

            // ability score/stats alignment list =====================================================
            int[] stats = new int[6];
            loopThrough = true;
            while (loopThrough == true)
            {
                Console.WriteLine("\nYour {0} will be rolled:\n", Enum.GetName(typeof(Selection), 4));
                for (int i = 0; i < Enum.GetNames(typeof(AbilityScore)).Length; i++)
                {
                    Console.WriteLine("(" + i + ") " + Enum.GetName(typeof(AbilityScore), i));
                }
                Console.ReadKey();
                Console.WriteLine("\n");

                // random selection for each stat
                RandomGen Stats = new RandomGen();
                for (int i = 0; i < Enum.GetNames(typeof(AbilityScore)).Length; i++)
                {
                    stats[i] = Stats.RandomStats();
                    Console.WriteLine(" " + stats[i] + " = {0}", Enum.GetName(typeof(AbilityScore), i));
                }

                Console.WriteLine("\nWould you like to keep these Ability Scores? y/n");

                // yes / no / wrong input loop ---- could put this into method
                bool loopThrough1 = true;
                while (loopThrough1 == true)
                {
                    input = Console.ReadLine();
                    if (input.Equals("y") || input.Equals("Y"))
                    {

                        loopThrough1 = false;
                        loopThrough = false;
                    }
                    else if (input.Equals("n") || input.Equals("N"))
                    {
                        raceSave = default(String);
                        loopThrough1 = false;
                        loopThrough = true;
                    }
                    else
                    {
                        Console.WriteLine("error, unknown input!!");
                        loopThrough1 = true;
                    }
                }
            }

            // character name menu list ===================================================

            String lName = default(String);
            String fName = default(String);

            int maleName = default(int);
            int femaleName = default(int);
            int lastName = default(int);

            int randomValue1 = default(int);

            String nameSave = default(String);
            loopThrough = true;
            while (loopThrough == true)
            {
                Console.WriteLine("\nChoose your Name, {0}:", raceSave);

                // switch statement to choose first name
                switch (raceValue)
                {
                    case 0:
                        // Dwarf
                        maleName = 0;
                        femaleName = 1;
                        for (int i = 0; i < 6; i++)
                        {
                            Console.WriteLine("{0, -20} {1, 4:N1}",("(" + i + ") " + Name[0, i]),("(" + (i + 6) + ") " + Name[1, i]));
                        }

                        break;
                    case 1:
                        // Elf
                        maleName = 3;
                        femaleName = 4;
                        for (int i = 0; i < 6; i++)
                        {
                            Console.WriteLine("{0, -20} {1, 4:N1}", ("(" + i + ") " + Name[3, i]), ("(" + (i + 6) + ") " + Name[4, i]));
                        }

                        break;
                    case 2:
                        // Halfling
                        maleName = 6;
                        femaleName = 7;
                        for (int i = 0; i < 6; i++)
                        {
                            Console.WriteLine("{0, -20} {1, 4:N1}", ("(" + i + ") " + Name[6, i]), ("(" + (i + 6) + ") " + Name[7, i]));
                        }

                        break;
                    case 3:
                        // Human
                        maleName = 9;
                        femaleName = 10;
                        for (int i = 0; i < 6; i++)
                        {
                            Console.WriteLine("{0, -20} {1, 4:N1}", ("(" + i + ") " + Name[9, i]), ("(" + (i + 6) + ") " + Name[10, i]));
                        }

                        break;
                }

                Console.WriteLine("\n\n # - Select\n X - Random");

                // loop for manual or random or wrong input
                while (loopThrough == true)
                {
                    // random selection input
                    input = Console.ReadLine();
                    if (input.Equals("x") || input.Equals("X"))
                    {
                        RandomGen rndName = new RandomGen();
                        randomValue = rndName.RandomCreate(0, 6);
                        randomValue1 = rndName.RandomCreate(maleName, femaleName);
                        fName = Name[randomValue1, randomValue];

                        loopThrough = false;
                    }
                    // manual selection input
                    else if (input.Equals("0") ||
                             input.Equals("1") ||
                             input.Equals("2") ||
                             input.Equals("3") ||
                             input.Equals("4") ||
                             input.Equals("5") ||
                             input.Equals("6") ||
                             input.Equals("7") ||
                             input.Equals("8") ||
                             input.Equals("9") ||
                             input.Equals("10") ||
                             input.Equals("11"))
                    {
                        loopThrough = false;
                    }
                    else
                    {
                        Console.WriteLine("error, unknown input!!");
                        loopThrough = true;
                    }




                }

                Console.WriteLine("\nChoose your Last Name, {0}:", raceSave);

                // switch statement to choose last name
                switch (raceValue)
                {
                    case 0:
                        // Dwarf
                        lastName = 2;
                        for (int i = 0; i < 6; i++)
                        {
                            Console.WriteLine("(" + i + ") " + Name[2, i]);
                        }

                        break;
                    case 1:
                        // Elf
                        lastName = 5;
                        for (int i = 0; i < 6; i++)
                        {
                            Console.WriteLine("(" + i + ") " + Name[5, i]);
                        }

                        break;
                    case 2:
                        // Halfling
                        lastName = 8;
                        for (int i = 0; i < 6; i++)
                        {
                            Console.WriteLine("(" + i + ") " + Name[8, i]);
                        }

                        break;
                    case 3:
                        // Human
                        lastName = 11;
                        for (int i = 0; i < 6; i++)
                        {
                            Console.WriteLine("(" + i + ") " + Name[11, i]);
                        }

                        break;
                }

                Console.WriteLine("\n\n # - Select\n X - Random");

                // loop for manual or random or wrong input
                loopThrough = true;
                while (loopThrough == true)
                {
                    // random selection input
                    input = Console.ReadLine();
                    if (input.Equals("x") || input.Equals("X"))
                    {
                        RandomGen rndName = new RandomGen();
                        randomValue = rndName.RandomCreate(0, 6);
                        lName = Name[lastName,randomValue];

                        loopThrough = false;
                    }
                    // manual selection input
                    else if (input.Equals("0") ||
                             input.Equals("1") ||
                             input.Equals("2") ||
                             input.Equals("3") ||
                             input.Equals("4") ||
                             input.Equals("5"))
                    {
                        loopThrough = false;
                    }
                    else
                    {
                        Console.WriteLine("error, unknown input!!");
                        loopThrough = true;
                    }
                }

                nameSave = fName + " " + lName;

                // rewrite this code for "a" and "an" before {0} like tonys code
                Console.WriteLine("Your name is {0}, would you like to keep this name? y/n", nameSave);


                // yes / no / wrong input loop ---- could put this into method
                bool loopThrough1 = true;
                while (loopThrough1 == true)
                {
                    input = Console.ReadLine();
                    if (input.Equals("y") || input.Equals("Y"))
                    {

                        loopThrough1 = false;
                        loopThrough = false;
                    }
                    else if (input.Equals("n") || input.Equals("N"))
                    {
                        raceSave = default(String);
                        loopThrough1 = false;
                        loopThrough = true;
                    }
                    else
                    {
                        Console.WriteLine("error, unknown input!!");
                        loopThrough1 = true;
                    }
                }
            }


            Console.ReadKey();






            Console.ReadKey();

        }
    }
}
