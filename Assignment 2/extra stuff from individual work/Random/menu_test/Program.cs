﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menu_test
{
    public enum Selection {Player, Race, Speed, Class, Level, Alignment, Ability_Scores};
    public enum Race { Dwarf, Elf, Halfling, Human };
    public enum Class { Cleric, Fighter, Rogue, Wizard };
    public enum Alignment { Lawful_good, Neutral_good, Chaotic_good, Lawful_neutral, Neutral, Chaotic_neutral, Lawful_evil, Neutral_evil, Chaotic_evil };
    public enum AbilityScore { Strength, Dexterity, Constitution, Intelligence, Wisdom, Charisma };
    //names
    public enum dwarfMNames { Thorin, Ulfgar, Dain, Darrak, Ragnar, Brottor };
    public enum dwarfFNames { Finellen, Amber, Ilde, Helja, Sannl, Hlin };
    public enum DwarfLNames { Battlehammer, Fineforge, Rumnaheim, Stoneskin, Frostbeard, Dankil };

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("===================== Dungeons and Dragons =====================");
            for (int i = 0; i < Enum.GetNames(typeof(Selection)).Length; i++)
            {
                Console.WriteLine("[" + i + "] " + Enum.GetName(typeof(Selection), i));
            }

           
            Console.ReadKey();
        }
    }
}
