﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menu_test1
{
    public class RandomGen
    {
        int rndGen;
        int min;
        int max;

        Random rnd = new Random();

        public int RandomCreate(int minInput, int maxInput)
        {
            min = minInput;
            max = maxInput;
            rndGen = rnd.Next(min, max);
            return rndGen;
        }

        public int RandomStats()
        {
            int d6_0 = rnd.Next(1, 6);
            int d6_1 = rnd.Next(1, 6);
            int d6_2 = rnd.Next(1, 6);
            int d6_3 = rnd.Next(1, 6);
            var list = new[] { d6_0, d6_1, d6_2, d6_3 };
            int minRoll = list.Min();
            rndGen = d6_0 + d6_1 + d6_2 + d6_3 - minRoll;
            return rndGen;
        }
    }
}
