﻿using System;
namespace Manual
{
    public class CCA  /*Create Character Attributes*/
    {


        public enum Race { Dwarf, Elf, Halfling, Human };
        public enum Classes { Cleric, Fighter, Rogue, Wizard };
        public enum Alignment
        {
            Lawful_good, Neutral_good, Chaotic_good,
            Lawful_neutral, Neutral, Chaotic_neutral, Lawful_evil, Neutral_evil, Chaotic_evil
        };
        public enum AbilityScore { Strength, Dexterity, Constitution, Intelligence, Wisdom, Charisma };

        //Human Male Name


        //Galishite
        public enum Galishite { GalishiteMale, GalishiteFemale, GalishiteSurnames };
        public enum GalishiteMale { Aseir, Bardeid, Haseid, Khemed, Mehmen, Sudeiman, Zasheir };
        public enum GalishiteFemale { Atala, Ceidil, Hama, Jasmal, Meilil, Seipora, Yasheira, Zasheida };
        public enum GalishiteSurnames { Basha, Dumein, Jassan, Khalid, Mostana, Pashar, Rein };


        //Chondathan
        public enum Chondathan { ChondathanMale, ChondathanFemale, ChondathanSurnames };
        public enum ChondathanMale { Darvin, Dorn, Evendur, Gorstag, Grim, Helm, Malark, Morn, Randal, Stedd };
        public enum ChondathanFemale { Arveene, Esvele, Jhessail, Kerri, Lureene, Miri, Rowan, Shandri, Tessele };
        public enum ChondathanSurnames { Amblecrown, Buckman, Dundragon, Evenwood, Greycastle, Tallstag };


        //Damaran
        public enum Damaran { DamaranMale, DamaranFemale, DamaranSurnames };
        public enum DamaranMale { Bor, Fodel, Glar, Grigor, Igan, Ivor, Kosef, Mival, Orel, Pavel, Sergor };
        public enum DamaranFemale { Alethra, Kara, Katernin, Mara, Natali, Olma, Tana, Zora };
        public enum DamaranSurnames { Bersk, Chernin, Dotsk, Kulenov, Marsk, Nemetsk, Shemov, Starag };


        //Rashemi
        public enum Rashemi { RashemiMale, RashemiFemale, RashemiSurnames };
        public enum RashemiMale { Borivik, Faurgar, Jandar, Kanithar, Madislak, Ralmevik, Shaumar, Vladislak };
        public enum RashemiFemale { Fyevarra, Hulmarra, Immith, Imzel, Navarra, Shevarra, Tammith, Yuldra };
        public enum RashemiSurnames { Chergoba, Dyernina, Iltazyara, Murnyethara, Stayanoga, Ulmokina };


        //Shou
        public enum Shou { ShouMale, ShouFemale, ShouSurname };
        public enum ShouMale { An, Chen, Chi, Fai, Jiang, Jun, Lian, Long, Meng, On, Shan, Shui, Wen };
        public enum ShouFemale { Bai, Chao, Jia, Lei, Mei, Qiao, Shui, Tai };
        public enum ShouSurname { Chien, Huang, Kao, Kung, Lao, Ling, Mei, Pin, Shin, Sum, Tan, Wan };


        //Turami
        public enum Turami { uramiMale, TuramiFemale, TuramiSurnames };
        public enum TuramiMale { Anton, Diero, Marcon, Pieron, Rimardo, Romero, Salazar, Umbero };
        public enum TuramiFemale { Balama, Dona, Faila, Jalana, Luisa, Marta, Quara, Selise, Vonda };
        public enum TuramiSurnames { Agosto, Astorio, Calabra, Domine, Falone, Marivaldi, Pisacar, Ramondo };






        public String CreateRace()
        {

            Console.WriteLine("Start to Choose Your Race: ");
            Console.WriteLine(
               "Choose your Race:\n0. {0}\n1. {1}\n2. {2}\n3. {3}\n",
                             Enum.GetName(typeof(Race), 0),
                             Enum.GetName(typeof(Race), 1),
                             Enum.GetName(typeof(Race), 2),
                             Enum.GetName(typeof(Race), 3));
            

            Boolean RaceCorrect = false;
            String inputRace = Console.ReadLine();


            while (RaceCorrect == false)
            {
                switch (inputRace)
                {
                    case "0":
                        inputRace = Enum.GetName(typeof(Race), 0);
                        RaceCorrect = true;
                        break;

                    case "1":
                        inputRace = Enum.GetName(typeof(Race), 1);
                        RaceCorrect = true;
                        break;

                    case "2":
                        inputRace = Enum.GetName(typeof(Race), 2);
                        RaceCorrect = true;
                        break;

                    case "3":
                        inputRace = Enum.GetName(typeof(Race), 3);
                        RaceCorrect = true;
                        break;

                    default:
                        Console.WriteLine("Wrong input, Please input correct");
                        break;
                }
                if (RaceCorrect == false)
                    inputRace = Console.ReadLine();
                else break;
            }
            return inputRace;
        }





        public String CreateClasses()
        {
            Console.WriteLine(
                "Start to Choose your Classes: ");

            Console.WriteLine(
                "Choose your Classes:\n0. {0}\n1. {1}\n2. {2}\n3. {3}\n",
            Enum.GetName(typeof(Classes), 0),
            Enum.GetName(typeof(Classes), 1),
            Enum.GetName(typeof(Classes), 2),
            Enum.GetName(typeof(Classes), 3));


            Boolean ClassesCorrect = false;
            String inputClasses = Console.ReadLine();


            while (ClassesCorrect == false)
            {
                switch (inputClasses)
                {
                    case "0":
                        inputClasses = Enum.GetName(typeof(Classes), 0);
                        ClassesCorrect = true;
                        break;

                    case "1":
                        inputClasses = Enum.GetName(typeof(Classes), 1);
                        ClassesCorrect = true;
                        break;

                    case "2":
                        inputClasses = Enum.GetName(typeof(Classes), 2);
                        ClassesCorrect = true;
                        break;

                    case "3":
                        inputClasses = Enum.GetName(typeof(Classes), 3);
                        ClassesCorrect = true;
                        break;

                    default:
                        Console.WriteLine("Wrong input, Please input correct");
                        break;
                }
                if (ClassesCorrect == false)
                    inputClasses = Console.ReadLine();
                else break;
            }
            return inputClasses;
        }







        public String CreateAlignment()
        {

            Console.WriteLine(
            "Start to Choose your Alignment:");

            Console.WriteLine(
                "Choose your AbilityScore:\n0. {0}\n1. {1}\n2. {2}\n3. {3}\n4. {4}\n5. {5}\n6. {6}\n7. {7}\n8. {8}\n",
            Enum.GetName(typeof(Alignment), 0),
            Enum.GetName(typeof(Alignment), 1),
            Enum.GetName(typeof(Alignment), 2),
            Enum.GetName(typeof(Alignment), 3),
            Enum.GetName(typeof(Alignment), 4),
            Enum.GetName(typeof(Alignment), 5),
            Enum.GetName(typeof(Alignment), 6),
            Enum.GetName(typeof(Alignment), 7),
            Enum.GetName(typeof(Alignment), 8));



            Boolean AlignmentCorrect = false;
            String inputAlignment = Console.ReadLine();


            while (AlignmentCorrect == false)
            {
                switch (inputAlignment)
                {
                    case "0":
                        inputAlignment = Enum.GetName(typeof(Alignment), 0);
                        AlignmentCorrect = true;
                        break;

                    case "1":
                        inputAlignment = Enum.GetName(typeof(Alignment), 1);
                        AlignmentCorrect = true;
                        break;

                    case "2":
                        inputAlignment = Enum.GetName(typeof(Alignment), 2);
                        AlignmentCorrect = true;
                        break;

                    case "3":
                        inputAlignment = Enum.GetName(typeof(Alignment), 3);
                        AlignmentCorrect = true;
                        break;

                    case "4":
                        inputAlignment = Enum.GetName(typeof(Alignment), 4);
                        AlignmentCorrect = true;
                        break;

                    case "5":
                        inputAlignment = Enum.GetName(typeof(Alignment), 5);
                        AlignmentCorrect = true;
                        break;

                    case "6":
                        inputAlignment = Enum.GetName(typeof(Alignment), 6);
                        AlignmentCorrect = true;
                        break;

                    case "7":
                        inputAlignment = Enum.GetName(typeof(Alignment), 7);
                        AlignmentCorrect = true;
                        break;

                    case "8":
                        inputAlignment = Enum.GetName(typeof(Alignment), 8);
                        AlignmentCorrect = true;
                        break;


                    default:
                        Console.WriteLine("Wrong input, Please input correct");
                        break;
                }
                if (AlignmentCorrect == false)
                    inputAlignment = Console.ReadLine();
                else break;
            }
            return inputAlignment;
        }






        public String SetAbility()
        {
            Console.WriteLine("Start to Set Your AbilityScore: ");
            Console.WriteLine(
                "Choose your Race:\n0. {0}\n1. {1}\n2. {2}\n3. {3}\n4. {4}\n5. {5}\n",
                            Enum.GetName(typeof(AbilityScore), 0),
                            Enum.GetName(typeof(AbilityScore), 1),
                            Enum.GetName(typeof(AbilityScore), 2),
                            Enum.GetName(typeof(AbilityScore), 3),
                            Enum.GetName(typeof(AbilityScore), 4),
                            Enum.GetName(typeof(AbilityScore), 5));

            Boolean AbilityCorrect = false;
            String inputAbility = Console.ReadLine();


            while (AbilityCorrect == false)
            {
                switch (inputAbility)
                {
                    case "0":
                        inputAbility = Enum.GetName(typeof(AbilityScore), 0);
                        AbilityCorrect = true;
                        break;

                    case "1":
                        inputAbility = Enum.GetName(typeof(AbilityScore), 1);
                        AbilityCorrect = true;
                        break;

                    case "2":
                        inputAbility = Enum.GetName(typeof(AbilityScore), 2);
                        AbilityCorrect = true;
                        break;

                    case "3":
                        inputAbility = Enum.GetName(typeof(AbilityScore), 3);
                        AbilityCorrect = true;
                        break;

                    case "4":
                        inputAbility = Enum.GetName(typeof(AbilityScore), 3);
                        AbilityCorrect = true;
                        break;

                    case "5":
                        inputAbility = Enum.GetName(typeof(AbilityScore), 3);
                        AbilityCorrect = true;
                        break;

                    default:
                        Console.WriteLine("Wrong input, Please input correct");
                        break;
                }
                if (AbilityCorrect == false)
                    inputAbility = Console.ReadLine();
                else break;
            }
            return inputAbility;
        }






        public String SetAbilityScore()
        {

            Console.WriteLine("Now input your ability source");

            Boolean ScoreCorrect = false;
            String inputAbilityScore = Console.ReadLine();

            while (ScoreCorrect == false)
            {
                switch (inputAbilityScore)
                {
                    case "1":
                        inputAbilityScore = "1";
                        ScoreCorrect = true;
                        break;

                    case "2":
                        inputAbilityScore = "2";
                        ScoreCorrect = true;
                        break;

                    case "3":
                        inputAbilityScore = "3";
                        ScoreCorrect = true;
                        break;

                    case "4":
                        inputAbilityScore = "4";
                        ScoreCorrect = true;
                        break;

                    case "5":
                        inputAbilityScore = "5";
                        ScoreCorrect = true;
                        break;

                    case "6":
                        inputAbilityScore = "6";
                        ScoreCorrect = true;
                        break;

                    default:
                        Console.WriteLine("Wrong input, Please input correct");
                        break;
                }
                if (ScoreCorrect == false)
                    inputAbilityScore = Console.ReadLine();
                else break;
            }
            return inputAbilityScore;
        }
       


        public void run()
        {
            CCA hero = new CCA();

            hero.CreateRace();
            Console.WriteLine("\n");

            hero.CreateClasses();
            Console.WriteLine("\n");

            hero.CreateAlignment();
            Console.WriteLine("\n");

            hero.SetAbility();
            Console.WriteLine("\n");

            hero.SetAbilityScore();
            Console.WriteLine("\n");

            Console.WriteLine("Your race is: " + hero.CreateRace() + "\n" + 
                              "Your Classes is: " + hero.CreateClasses() + "\n" + 
                              "Your Alignment is: " + hero.CreateAlignment() + "\n" +
                              "Your Ability is: " + hero.SetAbility() + "\n" + 
                              "Your Ability Score is: " + hero.SetAbilityScore());
        }
      

    }

}