﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Serialisation
{
    class Program
    {
        static void Main(string[] args)
        {
            Manager manager = new Manager();
            manager.Run();
        }
    }
}
