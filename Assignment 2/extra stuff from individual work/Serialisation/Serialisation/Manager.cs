﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
namespace Serialisation
{
    public class Manager
    {
        Stream stream;
        BinaryFormatter binaryFormatter;

        public void Run()
        {
            bool check;
            int playerChosen;
            DirectoryInfo playerSavesDir = new DirectoryInfo("Players/");
            FileInfo[] playerSaves = playerSavesDir.GetFiles("*.dat");

            //Asks Player to make the choice between different save files
            Console.WriteLine("Choose from 0 - " + (playerSaves.Length));
            for (int i = 0; i < playerSaves.Length; i++)
            {
                Console.WriteLine("(" + i + ")" + Load(playerSaves[i].Name).GetName());
            }
            Console.WriteLine("(" + playerSaves.Length + ")EXIT");

            //Continue to do this code until the player chose to EXIT
            do
            {
                //Check if the string is an integer and is within the choices
                do
                {
                    check = Int32.TryParse(Console.ReadLine(), out playerChosen);
                } while ((check == false) || (playerChosen < 0) || (playerChosen > playerSaves.Length));
                //Only do this if they have chosen a file and not EXIT
                if (playerChosen < playerSaves.Length)
                {
                    SavePlayer player = Load(playerSaves[playerChosen].Name);
                    Console.WriteLine(player.ToString());
                }
            } while (playerChosen != playerSaves.Length);
            Console.WriteLine("Goodbye :(");
            Console.ReadLine();
        }

        public void Save(SavePlayer name)
        {
            stream = File.Open("Players/" + name.GetName() + ".dat", FileMode.Create);
            binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(stream, name);
            stream.Close();
        }

        public SavePlayer Load(String file)
        {
            stream = File.Open("Players/" + file, FileMode.Open);
            binaryFormatter = new BinaryFormatter();
            SavePlayer player = (SavePlayer)binaryFormatter.Deserialize(stream);
            stream.Close();
            return player;  
        }

    }
}
