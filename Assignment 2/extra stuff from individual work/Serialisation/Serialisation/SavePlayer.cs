﻿using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;

namespace Serialisation
{
    [Serializable()]
    public class SavePlayer : ISerializable
    {
        private string name, race, pClass;

        public SavePlayer(string name, string race, string pClass)
        {
            this.name = name;
            this.race = race;
            this.pClass = pClass;
        }

        public SavePlayer(string name)
        {
            this.name = name;
        }
        public SavePlayer(SerializationInfo info, StreamingContext context)
        {
            name = (string)info.GetValue("Name", typeof(string));
            race = (string)info.GetValue("Race", typeof(string));
            pClass = (string)info.GetValue("Class", typeof(string));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", name);
            info.AddValue("Race", race);
            info.AddValue("Class", pClass);
        }

        public new string ToString()
        {
            StringComparison comparison = StringComparison.InvariantCulture;
            string a;
            if (name.StartsWith("A", comparison) == true || name.StartsWith("E", comparison) == true || name.StartsWith("U", comparison) == true || name.StartsWith("I", comparison) == true || name.StartsWith("O", comparison) == true)
            {
                a = "an";
            }
            else
            {
                a = "a";
            }
            return "I am " + name + ", " + a + " " + pClass + " " + race;
        }

        public string GetName()
        {
            return name;
        }

    }
}
