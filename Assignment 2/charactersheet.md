---basics from the D&D guide for character creation--
RACE
  dwarf
  elf
  halfling
  human
CLASS
  Cleric
    A priestly champion who wields divine magic in service of a higher power
    Hit die
      d8
    Ability
      Wisdom
    SavingThrow prof.
      Wisdom
      Charisma
    Skills
      Choose 2 from
        History
        Insight
        Medicine
        Persuasion
        Religion
    Equipment prof.
      Armour
        Light
        Medium
      Weapons
        simple weapons
        shields  
    Spells
      prof. bonus
        +2
      Features
        spellcasting
        divine domain
      Cantrips known
        3
      Spell slots per spell level
        1st
          2

  Fighter
    A master of martial combat skilled with a variety of weapons and armour
    Hit die
      d10
    Ability
      Strength/Dexterity (choice)
    SavingThrow prof.
      Strength
      Constitution
    Equipment prof.
      Armour
        All-
          Light
          Medium
          Heavy
      Weapons
        simple weapons
        martial weapons
        shields    
  Rogue
    A scoundrel who uses stealth and trickery to overcome obstacles and enemies
    Hit die
      d8
    Ability
      Dexterity
    SavingThrow prof.
      Dexterity
      Intelligence
    Equipment prof.
      Armour
        Light
      Weapons
        simple weapons
        hand crossbows
        longswords
        rapiers
        shortswords
  Wizard
    A scholarly magic-user capable of manipulating the structures of reality
    Hit die
      d6
    Ability
      Intelligence
    SavingThrow prof.
      Intelligence
      Wisdom
    Equipment prof.
      Armour
        -None
      Weapons
        daggers
        darts
        slings
        quarterstaffs
        light crossbows
LEVEL
  1st
