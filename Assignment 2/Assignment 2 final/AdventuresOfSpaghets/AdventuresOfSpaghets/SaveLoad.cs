﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
// using System.Xml.Serialization;
namespace AdventuresOfSpaghets
{
    public class SaveLoad
    {
        private Stream stream;
        private BinaryFormatter binaryFormatter;
        private string dir;

        public SaveLoad()
        {
            dir = Environment.CurrentDirectory;
            try
            {
                dir = dir.Substring(0, dir.IndexOf("bin")) + "\\Players\\";
            }
            catch (ArgumentOutOfRangeException)
            {
                dir += "/Players/";
            }
            catch (FileNotFoundException)
            {
                dir = dir.Substring(0, dir.IndexOf("bin"));
                Path.Combine(dir, "Players");
                try
                {
                    dir = dir.Substring(0, dir.IndexOf("bin")) + "\\Players\\";
                }
                catch (FileNotFoundException)
                {
                    dir = dir.Substring(0, dir.IndexOf("bin")) + "/Players/";
                }
            }
        }

        public string getDir()
        {
            return dir;
        }
        //public void Run()
        //{
        //    bool check;
        //    int playerChosen;
        //    DirectoryInfo playerSavesDir = new DirectoryInfo("Players/");
        //    FileInfo[] playerSaves = playerSavesDir.GetFiles("*.dat");

        //    //Asks Player to make the choice between different save files
        //    Console.WriteLine("Choose from 0 - " + (playerSaves.Length));
        //    for (int i = 0; i < playerSaves.Length; i++)
        //    {
        //        Console.WriteLine("(" + i + ")" + Load(playerSaves[i].Name).GetName());
        //    }
        //    Console.WriteLine("(" + playerSaves.Length + ")EXIT");

        //    //Continue to do this code until the player chose to EXIT
        //    do
        //    {
        //        //Check if the string is an integer and is within the choices
        //        do
        //        {
        //            check = Int32.TryParse(Console.ReadLine(), out playerChosen);
        //        } while ((check == false) || (playerChosen < 0) || (playerChosen > playerSaves.Length));
        //        //Only do this if they have chosen a file and not EXIT
        //        if (playerChosen < playerSaves.Length)
        //        {
        //            Player player = Load(playerSaves[playerChosen].Name);
        //            Console.WriteLine(player.ToString());
        //        }
        //    } while (playerChosen != playerSaves.Length);
        //    Console.WriteLine("Goodbye :(");
        //    Console.ReadLine();
        //}

        public void Save(Player name)
        {
            stream = File.Open(dir + name.GetName() + ".dat", FileMode.Create);
            binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(stream, name);
            stream.Close();
        }

        public Player Load(String file)
        {
            stream = File.Open(dir + file, FileMode.Open);
            binaryFormatter = new BinaryFormatter();
            Player player = (Player)binaryFormatter.Deserialize(stream);
            stream.Close();
            return player;
        }

    }
}
