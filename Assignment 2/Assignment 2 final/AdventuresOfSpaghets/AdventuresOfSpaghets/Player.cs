﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;

namespace AdventuresOfSpaghets
{
    [Serializable()]
    public class Player
    {
        private string name;
        private string alignment;
        private string race;
        private string pClass;
        private enum Stats {STR, DEX, CON, INT, WIS, CHA}
        private int[] stats;
        private int speed;
        private int uSpeed = 0;
        private int[] uStats = { 0, 0, 0, 0, 0, 0 };
        private List<string> spells;
        private List<string> skills;

        public Player(string name, string alignment, string race, string pClass, int[] stats, int speed)
        {
            this.name = name;
            this.alignment = alignment;
            this.race = race;
            this.pClass = pClass;
            this.stats = stats;
            this.speed = speed;
            spells = new List<string>();
            skills = new List<string>();
        }

        //Created a constructer for saving and loading
        public Player(SerializationInfo info, StreamingContext context)
        {
            name = (string)info.GetValue("Name", typeof(string));
            race = (string)info.GetValue("Race", typeof(string));
            pClass = (string)info.GetValue("Class", typeof(string));
            stats = (int[])info.GetValue("Stats", typeof(int[]));
            uStats = (int[])info.GetValue("Update Stats", typeof(int[]));
            spells = (List<string>)info.GetValue("Spells", typeof(List<string>));
            skills = (List<string>)info.GetValue("Skills", typeof(List<string>));

        }

        public string GetName()
        {
            return name;
        }

        public void AddSpell(string spell)
        {
            spells.Add(spell);
        }

        public void DeleteSpell(string spell)
        {
            spells.Remove(spell);
        }

        public void AddSkill(string skill)
        {
            skills.Add(skill);
        }

        public void DeleteSkill(string skill)
        {
            skills.Remove(skill);
        }

        //Increment the stats (including speed)
        public void IncreaseStat(string a)
        {
            for (int i = 0; i < uStats.Length; i++)
            {
                if (Enum.GetName(typeof(Stats), i).Equals(a))
                {
                    uStats[i]++;
                    break;
                }
                else if (a.Equals("Speed"))
                {
                    uSpeed++;
                    break;
                }
            }
        }

        //Decrementing the stats(including speed)
        public void DecreaseStat(string a)
        {
            for (int i = 0; i < uStats.Length; i++)
            {
                if (Enum.GetName(typeof(Stats), i).Equals(a))
                {
                    uStats[i]--;
                    break;
                }
                else if (a.Equals("Speed"))
                {
                    uSpeed--;
                    break;
                }
            }
        }

        //Printing out all the information in a neat format
        public string GetDetails()
        {
            string details = String.Format("-----------------------------------------------\n{0, 12}{1}\n" +
                                        "{2, 12}{3}\n" +
                                        "{4, 12}{5}\n" +
                                        "{6, 12}{7}\n",
                                        "Name: ", name,
                                        "Alignment: ", alignment,
                                        "Race: ", race,
                                        "Class: ", pClass);
            for (int i = 0; i < stats.Length; i++)
            {

                details += String.Format("{0, 10}{1} {2, 2} {3:+ #;- #;+ 0}\n", Enum.GetName(typeof(Stats), i), ":", stats[i], uStats[i]);
                
            }

            details += String.Format("{0, 12}{1, 2} {2:+ #;- #;+ 0}\n", "Speed: ", speed, uSpeed);
            details += "    Spells: ";
            for(int i = 0; i < spells.Count;i++)
            {
                details += String.Format("{0}, ",spells[i]);
            }
            details += "\n    Skills: ";
            for (int i = 0; i < skills.Count; i++)
            {
                details += String.Format("{0}, ",skills[i]);
            }
            details += "\n-----------------------------------------------\n";
            return details;
        }

        //Printing out current spells acquired by the player
        public string GetCurrentSpells()
        {
            string spells = "";
            for (int i = 0; i < this.spells.Count; i++)
            {
                spells += i + ". " + this.spells[i]+ "\n";
            }
            return spells;
        }

        public void RemoveSpell()
        {
            string remove = "";
            int choice;
            for (int i = 0; i < 1; i++)
            {
                choice = Int32.Parse(Console.ReadLine());
                if (choice > spells.Count || choice < 0)
                {
                    Console.WriteLine("\nError\n");
                    i--;
                }
                else
                {
                    remove = spells[choice];
                }
            }
            spells.Remove(remove);
        }

        public void RemoveSkill()
        {
            string remove = "";
            int choice;
            for (int i = 0; i < 1; i++)
            {
                choice = Int32.Parse(Console.ReadLine());
                if (choice > skills.Count || choice < 0)
                {
                    Console.WriteLine("\nError\n");
                    i--;
                }
                else
                {
                    remove = skills[choice];
                }
            }
            skills.Remove(remove);
        }

        //Printing out current skills acquired by the player
        public string GetCurrentSkills()
        {
            string skills = "";
            for (int i = 0; i < this.skills.Count; i++)
            {
                skills += i + ". " + this.skills[i] + "\n";
            }
            return skills;
        }







    }
}
