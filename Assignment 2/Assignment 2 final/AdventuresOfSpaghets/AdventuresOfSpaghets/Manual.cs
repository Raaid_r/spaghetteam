﻿using System;
namespace AdventuresOfSpaghets
{
    public class Manual  /*Create Character Attributes*/
    {

        public enum Selection { Player, Race, Class, Alignment, Ability_Scores, Character_Name };
        public enum Race { Dwarf, Elf, Halfling, Human }; //25, 30, 25, 30
        public int speed;
        public enum Class { Cleric, Fighter, Rogue, Wizard };
        public enum Alignment
        {
            Lawful_good, Neutral_good, Chaotic_good,
            Lawful_neutral, Neutral, Chaotic_neutral, Lawful_evil, Neutral_evil, Chaotic_evil
        };
        public enum Ability { Strength, Dexterity, Constitution, Intelligence, Wisdom, Charisma };



        //Human
        String[,] Name = new String[12, 6]
        {
                // dwarves
                { "Thorin", "Ulfgar", "Dain", "Darrak", "Ragnar", "Brottor" },
                { "Finellen", "Amber", "Ilde", "Helja", "Sannl", "Hlin" },
                { "Battlehammer", "Fineforge", "Rumnaheim", "Stoneskin", "Frostbeard", "Dankil" },
                // elves
                { "Aeler", "Erevan", "Ivellios", "Paelias", "Tharivol", "Varis" },
                { "Adrie", "Birel", "Enna", "Keyleth", "Meriele", "Sariel" },
                { "Amakiir (Gemflower)", "Ilphelkiir (Gemblossom)", "Meliamne (Oakenheel)", "Liadon (Silverfrond)", "Siannodel (Moonbrook)", "Xiloscient (Goldpetal)" },
                // halflings
                { "Alton", "Cade", "Eldon", "Garret", "Milo", "Wellby"},
                { "Andry", "Callie", "Lavinia", "Nedda", "Vani", "Shaena"},
                { "Brushgather", "Goodbarrel", "Tosscobble", "Greenbottle", "Hilltopple", "Tealeaf"},
                // humans
                { "Haseid", "Stedd", "Fodel", "Lander", "Ramas", "Shaumar"},
                { "Atala", "Shandri", "Natali", "Kethra", "Tammith", "Jalana"},
                { "Marsk", "Windrivver", "Uuthrakt", "Iltazyara", "Ramondo", "Huang"}
        };


        public String CreateRace(String inputRace)
        {
            switch (inputRace)
            {
                case "0":
                    inputRace = Enum.GetName(typeof(Race), 0);
                    Console.WriteLine("Speed is 25");
                    break;

                case "1":
                    Console.WriteLine("Speed is 30");
                    inputRace = Enum.GetName(typeof(Race), 1);
                    break;

                case "2":
                    Console.WriteLine("Speed is 25");
                    inputRace = Enum.GetName(typeof(Race), 2);
                    break;
                case "3":
                    Console.WriteLine("Speed is 30");
                    inputRace = Enum.GetName(typeof(Race), 3);
                    break;
                default:
                    Console.WriteLine("Wrong input, Please input correct" + " +2 Constitution\n 25 base speed - not effected by heavy armour\n\n Dwarven Toughness:\n +2 Strength\n +1 max HP, +1 for each level\n\n Dark Vision:\n ~20m Dim Light seen as Bright Light\n ~20m Darkness seen as greyscaled Dim Light");
                    inputRace = "";
                    break;
            }
            return inputRace;
        }





        public String CreateClass(String inputClass)
        {
                switch (inputClass)
                {
                    case "0":
                        inputClass = Enum.GetName(typeof(Class), 0);
                        break;

                    case "1":
                        inputClass = Enum.GetName(typeof(Class), 1);
                        break;

                    case "2":
                        inputClass = Enum.GetName(typeof(Class), 2);
                        break;

                    case "3":
                        inputClass = Enum.GetName(typeof(Class), 3);
                        break;

                    default:
                        Console.WriteLine("Wrong input, Please input correct");
                        break;
            }
            return inputClass;
        }







        public String CreateAlignment(String inputAlignment)
        {

            switch (inputAlignment)
                {
                    case "0":
                        inputAlignment = Enum.GetName(typeof(Alignment), 0);
                        break;

                    case "1":
                        inputAlignment = Enum.GetName(typeof(Alignment), 1);
                        break;

                    case "2":
                        inputAlignment = Enum.GetName(typeof(Alignment), 2);
                        break;

                    case "3":
                        inputAlignment = Enum.GetName(typeof(Alignment), 3);
                        break;

                    case "4":
                        inputAlignment = Enum.GetName(typeof(Alignment), 4);
                        break;

                    case "5":
                        inputAlignment = Enum.GetName(typeof(Alignment), 5);
                        break;

                    case "6":
                        inputAlignment = Enum.GetName(typeof(Alignment), 6);
                        break;

                    case "7":
                        inputAlignment = Enum.GetName(typeof(Alignment), 7);
                        break;

                    case "8":
                        inputAlignment = Enum.GetName(typeof(Alignment), 8);
                        break;

                    default:
                        Console.WriteLine("Wrong input, Please input correct");
                        break;
            }
            return inputAlignment;
        }



        public String CreateAbility(String inputAbility)
        {
            switch (inputAbility)
            {
                case "0":
                    inputAbility = Enum.GetName(typeof(Ability), 0);
                    break;

                case "1":
                    inputAbility = Enum.GetName(typeof(Ability), 1);
                    break;

                case "2":
                    inputAbility = Enum.GetName(typeof(Ability), 2);
                    break;

                case "3":
                    inputAbility = Enum.GetName(typeof(Ability), 3);
                    break;

                case "4":
                    inputAbility = Enum.GetName(typeof(Ability), 4);
                    break;

                case "5":
                    inputAbility = Enum.GetName(typeof(Ability), 5);
                    break;

                default:
                    Console.WriteLine("Wrong input, Please input correct");
                    break;
            }
            return inputAbility;
        }



            public String SetAbilityScore(String inputAbilityScore)
            {
                    switch (inputAbilityScore)
                    {
                        case "1":
                            inputAbilityScore = "1";
                            break;

                        case "2":
                            inputAbilityScore = "2";
                            break;

                        case "3":
                            inputAbilityScore = "3";
                            break;

                        case "4":
                            inputAbilityScore = "4";
                            break;

                        case "5":
                            inputAbilityScore = "5";
                            break;

                        case "6":
                            inputAbilityScore = "6";
                            break;

                        default:
                            Console.WriteLine("Wrong input, Please input correct");
                            break;
                }
                return inputAbilityScore;
            }



        public String CreatefName(String inputName, int maleRace, int femaleRace)
        {
            String fName = default(String);
            switch (inputName)
            {
                case "0":
                    fName = Name[maleRace, 0];
                    break;
                case "1":
                    fName = Name[maleRace, 1];
                    break;
                case "2":
                    fName = Name[maleRace, 2];
                    break;
                case "3":
                    fName = Name[maleRace, 3];
                    break;
                case "4":
                    fName = Name[maleRace, 4];
                    break;
                case "5":
                    fName = Name[maleRace, 5];
                    break;
                case "6":
                    fName = Name[femaleRace, 0];
                    break;
                case "7":
                    fName = Name[femaleRace, 1];
                    break;
                case "8":
                    fName = Name[femaleRace, 2];
                    break;
                case "9":
                    fName = Name[femaleRace, 3];
                    break;
                case "10":
                    fName = Name[femaleRace, 4];
                    break;
                case "11":
                    fName = Name[femaleRace, 5];
                    break;
            }
            return fName;
        }

        public String CreatelName(String inputName, int Race)
        {
            String lName = default(String);
            switch (inputName)
            {
                case "0":
                    lName = Name[Race, 0];
                    break;
                case "1":
                    lName = Name[Race, 1];
                    break;
                case "2":
                    lName = Name[Race, 2];
                    break;
                case "3":
                    lName = Name[Race, 3];
                    break;
                case "4":
                    lName = Name[Race, 4];
                    break;
                case "5":
                    lName = Name[Race, 5];
                    break;
            }
            return lName;
        }


    }

}