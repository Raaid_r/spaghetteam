﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventuresOfSpaghets
{
    class TraitInfo
    {

        public string RaceInfo(String race)
        {
            String Info = default(String);

            switch (race)
            {
                case "Dwarf":
                    // Dwarf
                    Info = " +2 Constitution\n" +
                        " 25 base speed - not effected by heavy armour\n\n" +
                        " Dwarven Toughness:\n +2 Strength\n +1 max HP, +1 HP for each level\n\n Dark Vision:\n" +
                        " ~20m Dim Light seen as Bright Light\n ~20m Darkness seen as greyscaled Dim Light";
                    break;
                case "Elf":
                    // Elf
                    Info = " +2 Dexterity\n" +
                        " 30 base speed\n\n" +
                        " Mask of the Wild:\n +1 Wisdom\n" +
                        "You can attempt to hide even when you are only lightly obscured by foliage, heavy rain, falling snow, mist, and other natural phenomena.\n\n" +
                        " Dark Vision:\n ~20m Dim Light seen as Bright Light\n ~20m Darkness seen as greyscaled Dim Light";
                    break;
                case "Halfling":
                    // Halfling
                    Info = " +2 Dexterity\n" +
                        " 25 base speed\n\n" +
                        " Lightfoot:\n +1 Charisma\n You can move through the space of any creature that is a size larger than you\n" +
                        " You can attempt to hide even when only obscured by a creature that is at least one size larger than you\n\n" +
                        " Lucky:\n When you\n  attack roll\n  ability check\n  saving throw\n you can re-roll but must use the new roll";
                    break;
                case "Human":
                    // Human
                    Info = " 30 base speed\n\n" +
                        " +1 Strength\n" +
                        " +1 Dexterity\n" +
                        " +1 Constitution\n" +
                        " +1 Intelligence\n" +
                        " +1 Wisdom\n" +
                        " +1 Charisma";
                    break;
            }

            return Info;
        }

        public string ClassInfo(String classInfo)
        {
            String Info = default(String);

            switch (classInfo)
            {
                case "Cleric":
                    // Cleric
                    Info = "A priestly champion who wields divine magic in service of a higher power.\n" +
                        " Hit Die: d8\n" +
                        " Primary Ability: Wisdom\n Prof. in: Wisdom and Charisma";
                    break;
                case "Fighter":
                    // Fighter
                    Info = "A master of martial combat, skilled with a variety of weapons and armor.\n" +
                        " Hit Die: d10\n" +
                        " Primary Ability: Strength/Dexterity\n Prof. in: Strength and Constitution";
                    break;
                case "Rogue":
                    // Rogue
                    Info = "A scoundrel who uses stealth and trickery to overcome obstacles and enemies.\n" +
                        " Hit Die: d8\n" +
                        " Primary Ability: Dexterity\n Prof. in: Dexterity and Intelligence";
                    break;
                case "Wizard":
                    // Wizard
                    Info = "A scholarly magic-user capable of manipulating the structures of reality.\n" +
                        " Hit Die: d6\n" +
                        " Primary Ability: Intelligence\n Prof. in: Intelligence and Wisdom";
                    break;
            }

            return Info;
        }

        public string AlignmentInfo(String alignment)
        {
            String Info = default(String);

            switch (alignment)
            {
                case "Lawful_good":
                    // Lawful Good
                    Info = "creatures can be counted on to do the right thing as expected by society.";
                    break;
                case "Neutral_good":
                    // Neutral Good
                    Info = "folk do the best they can to help others according to their needs.";
                    break;
                case "Chaotic_good":
                    // Chaotic Good
                    Info = "creatures act as their conscience directs, with little regard for what others expect.";
                    break;
                case "Lawful_neutral":
                    // Lawful Neutral
                    Info = "individuals act in accordance with law, tradition, or personal codes.";
                    break;
                case "Neutral":
                    // Neutral
                    Info = "is the alignment of those who prefer to steer clear of moral questions and don’t take sides, doing what seems best at the time.";
                    break;
                case "Chaotic_neutral":
                    // Chaotic Neutral
                    Info = "creatures follow their whims, holding their personal freedom above all else.";
                    break;
                case "Lawful_evil":
                    // Lawful Evil
                    Info = "creatures methodically take what they want, within the limits of a code of tradition, loyalty, or order.";
                    break;
                case "Neutral_evil":
                    // Neutral Evil
                    Info = "is the alignment of those who do whatever they can get away with, without compassion or qualms.";
                    break;
                case "Chaotic_evil":
                    // Chaotic Evil
                    Info = "creatures act with arbitrary violence, spurred by their greed, hatred, or bloodlust.";
                    break;
            }

            return Info;
        }


    }
}
