﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
// using System.Xml.Serialization;

namespace AdventuresOfSpaghets
{
    public class Menu
    {
        private string chosen;
        private int playerChosen;
        private DirectoryInfo playerSavesDir;
        private FileInfo[] playerSaves;
        private SaveLoad saveLoad;
        private DnDSkills dnDSkills;
        private Spells spells;
        private Manual manual;
        private Random1 random;
        private TraitInfo info;
        private Player player;
        private bool loop;
        private bool edit;
        private string menuChoice;
        private int multiply;
        private string statChoice;
        private string spellChoice;
        private string name;
        private string alignment;
        private string race;
        private string pClass;
        private int[] stats;
        private int speed;
        private int raceValue = default(int);
        private bool loopThrough = true;

        // names
        String[,] Name = new String[12, 6]
        {
                // dwarves
                { "Thorin", "Ulfgar", "Dain", "Darrak", "Ragnar", "Brottor" },
                { "Finellen", "Amber", "Ilde", "Helja", "Sannl", "Hlin" },
                { "Battlehammer", "Fineforge", "Rumnaheim", "Stoneskin", "Frostbeard", "Dankil" },
                // elves
                { "Aeler", "Erevan", "Ivellios", "Paelias", "Tharivol", "Varis" },
                { "Adrie", "Birel", "Enna", "Keyleth", "Meriele", "Sariel" },
                { "Amakiir (Gemflower)", "Ilphelkiir (Gemblossom)", "Meliamne (Oakenheel)", "Liadon (Silverfrond)", "Siannodel (Moonbrook)", "Xiloscient (Goldpetal)" },
                // halflings
                { "Alton", "Cade", "Eldon", "Garret", "Milo", "Wellby"},
                { "Andry", "Callie", "Lavinia", "Nedda", "Vani", "Shaena"},
                { "Brushgather", "Goodbarrel", "Tosscobble", "Greenbottle", "Hilltopple", "Tealeaf"},
                // humans
                { "Haseid", "Stedd", "Fodel", "Lander", "Ramas", "Shaumar"},
                { "Atala", "Shandri", "Natali", "Kethra", "Tammith", "Jalana"},
                { "Marsk", "Windrivver", "Uuthrakt", "Iltazyara", "Ramondo", "Huang"}
        };

        public enum Selection { Player, Race, Class, Alignment, Ability_Scores, Character_Name };
        public enum AbilityScore { Strength, Dexterity, Constitution, Intelligence, Wisdom, Charisma };

        public Menu()
        {
            saveLoad = new SaveLoad();
            playerSavesDir = new DirectoryInfo(saveLoad.getDir());
            dnDSkills = new DnDSkills();
            spells = new Spells();
            loop = true;
            manual = new Manual();
            random = new Random1();
            raceValue = default(int);
            stats = new int[6];
            info = new TraitInfo();
        }

        public void Run()
        {
            do
            {
                playerSaves = playerSavesDir.GetFiles("*.dat");
                Console.Clear();
                try
                {
                    Console.SetWindowSize(120, 32);
                }
                catch (PlatformNotSupportedException)
                {
                }
                Console.WriteLine(" ");
                Console.WriteLine("   ▄████████ ████████▄   ▄█    █▄     ▄████████ ███▄▄▄▄       ███     ███    █▄     ▄████████    ▄████████    ▄████████");
                Console.WriteLine("  ███    ███ ███   ▀███ ███    ███   ███    ███ ███▀▀▀██▄ ▀█████████▄ ███    ███   ███    ███   ███    ███   ███    ███");
                Console.WriteLine("  ███    ███ ███    ███ ███    ███   ███    █▀  ███   ███    ▀███▀▀██ ███    ███   ███    ███   ███    █▀    ███    █▀ ");
                Console.WriteLine("  ███    ███ ███    ███ ███    ███  ▄███▄▄▄     ███   ███     ███   ▀ ███    ███  ▄███▄▄▄▄██▀  ▄███▄▄▄       ███       ");
                Console.WriteLine("▀███████████ ███    ███ ███    ███ ▀▀███▀▀▀     ███   ███     ███     ███    ███ ▀▀███▀▀▀▀▀   ▀▀███▀▀▀     ▀███████████");
                Console.WriteLine("  ███    ███ ███    ███ ███    ███   ███    █▄  ███   ███     ███     ███    ███ ▀███████████   ███    █▄           ███");
                Console.WriteLine("  ███    ███ ███   ▄███ ███    ███   ███    ███ ███   ███     ███     ███    ███   ███    ███   ███    ███    ▄█    ███");
                Console.WriteLine("  ███    █▀  ████████▀   ▀██████▀    ██████████  ▀█   █▀     ▄████▀   ████████▀    ███    ███   ██████████  ▄████████▀ ");
                Console.WriteLine("                                                                                   ███    ███                          ");
                Console.WriteLine("                                              ▄██████▄     ▄████████                                                   ");
                Console.WriteLine("                                             ███    ███   ███    ███                                                   ");
                Console.WriteLine("                                             ███    ███   ███    █▀                                                    ");
                Console.WriteLine("                                             ███    ███  ▄███▄▄▄                                                       ");
                Console.WriteLine("                                             ███    ███ ▀▀███▀▀▀                                                       ");
                Console.WriteLine("                                             ███    ███   ███                                                          ");
                Console.WriteLine("                                             ███    ███   ███                                                          ");
                Console.WriteLine("                                              ▀██████▀    ███                                                          ");
                Console.WriteLine("                                                                                                                       ");
                Console.WriteLine("        ▄████████    ▄███████▄    ▄████████    ▄██████▄     ▄█    █▄       ▄████████     ███        ▄████████          ");
                Console.WriteLine("       ███    ███   ███    ███   ███    ███   ███    ███   ███    ███     ███    ███ ▀█████████▄   ███    ███          ");
                Console.WriteLine("       ███    █▀    ███    ███   ███    ███   ███    █▀    ███    ███     ███    █▀     ▀███▀▀██   ███    █▀           ");
                Console.WriteLine("       ███          ███    ███   ███    ███  ▄███         ▄███▄▄▄▄███▄▄  ▄███▄▄▄         ███   ▀   ███                 ");
                Console.WriteLine("     ▀███████████ ▀█████████▀  ▀███████████ ▀▀███ ████▄  ▀▀███▀▀▀▀███▀  ▀▀███▀▀▀         ███     ▀███████████          ");
                Console.WriteLine("              ███   ███          ███    ███   ███    ███   ███    ███     ███    █▄      ███              ███          ");
                Console.WriteLine("        ▄█    ███   ███          ███    ███   ███    ███   ███    ███     ███    ███     ███        ▄█    ███          ");
                Console.WriteLine("      ▄████████▀   ▄████▀        ███    █▀    ████████▀    ███    █▀      ██████████    ▄████▀    ▄████████▀           ");
                Console.WriteLine("                                                                                                                       ");
                Console.WriteLine("                                                (c)reate Character                                                     ");
                Console.WriteLine("                                                 (l)oad Character                                                      ");
                Console.WriteLine("                                                     (e)xit                                                            ");
                //Makes sure the user chooses between C and L
                do
                {
                    chosen = Console.ReadLine();
                } while (!chosen.Equals("c") && !chosen.Equals("l") && !chosen.Equals("e"));

                //Player chooses to Create Character
                if (chosen.Equals("c"))
                {
                    Console.Clear();
                    Create();
                }

                //Player chooses to Load Characater
                else if (chosen.Equals("l"))
                {
                    if (playerSaves.Length == 0)
                    {
                        Console.WriteLine("ERROR: Must create a new player first!");
                        Console.ReadLine();
                    }
                    else
                    {
                        Console.Clear();
                        Load();
                    }
                }
            } while (!chosen.Equals("e"));
            Console.ReadLine();
        }

        private void Create()
        {


            //menu list showing each trait to be created

            Console.WriteLine("\nEnter your traits:\n");
            for (int i = 0; i < Enum.GetNames(typeof(Selection)).Length; i++)
            {
                Console.WriteLine("(" + i + ") " + Enum.GetName(typeof(Selection), i));
            }
            Console.WriteLine("\n\nany key to continue");
            Console.ReadKey();


            //Create your race
            while (loopThrough == true)
            {
                chosen = Ask("Please Enter Your Race: \n(0) Dwarf \n(1) Elf \n(2) Halfing \n(3) Human \n\nX - Random\n");
                // random selector grabs number between parameters of Race and saves value to race and speed, exiting loop
                if (chosen.Equals("X") || chosen.Equals("x"))
                {
                    race = random.CreateRace();
                    speed = random.CreateSpeed();
                    Console.WriteLine("Your race is {0}", race);
                    Console.ReadLine();
                    loopThrough = false;
                }
                // manual selector allows for number between race parameters and saves, exiting loop
                else if (chosen.Equals("0") ||
                         chosen.Equals("1") ||
                         chosen.Equals("2") ||
                         chosen.Equals("3"))
                {
                    race = manual.CreateRace(chosen);
                    loopThrough = false;

                }
                // loops until either manual or random generation is selected
                else
                {
                    race = "HIYA";
                    loopThrough = true;
                }
            }

            //shows information for the chosen race
            Console.WriteLine(info.RaceInfo(race));
            Console.ReadLine();

            //Create your class
            loopThrough = true;
            while (loopThrough == true)
            {
                chosen = Ask("Please Enter Your Class: \n(0) Cleric \n(1) Fighter \n(2) Rogue \n(3) Wizard \n\nX - Random\n");
                // random
                if (chosen.Equals("X") || chosen.Equals("x"))
                {
                    pClass = random.CreateClass();
                    Console.WriteLine("Your class is {0}", pClass);
                    Console.ReadLine();
                    loopThrough = false;
                }
                // manual
                else if (chosen.Equals("0") ||
                         chosen.Equals("1") ||
                         chosen.Equals("2") ||
                         chosen.Equals("3"))
                {
                    pClass = manual.CreateClass(chosen);
                    loopThrough = false;
                }
                else
                {
                    pClass = "HIYA";
                    loopThrough = true;
                }
            }

            //shows information for the chosen class
            Console.WriteLine(info.ClassInfo(pClass));
            Console.ReadLine();

            //Create your Alignment 
            loopThrough = true;
            while (loopThrough == true)
            {
                chosen = Ask("Please Enter Your Alignment: \n(0) Lawful_good \n(1) Neutral_good \n(2) Chaotic_good \n(3) Lawful_neutral \n(4) Neutral \n(5) Chaotic_neutral \n(6)  Lawful_evil \n(7)  Neutral_evil \n(8) Chaotic_evil \n\nX - Random\n");
                // random
                if (chosen.Equals("X") || chosen.Equals("x"))
                {
                    alignment = random.CreateAlignment();
                    Console.WriteLine("Your Alignment is {0}", alignment);
                    loopThrough = false;
                    Console.ReadLine();
                }
                // manual
                else if (chosen.Equals("0") ||
                        chosen.Equals("1") ||
                        chosen.Equals("2") ||
                        chosen.Equals("3") ||
                        chosen.Equals("4") ||
                        chosen.Equals("5") ||
                        chosen.Equals("6") ||
                        chosen.Equals("7") ||
                        chosen.Equals("8"))
                {
                    alignment = manual.CreateAlignment(chosen);
                    loopThrough = false;
                }
                else
                {
                    alignment = "HIYA";
                    loopThrough = true;
                }
            }

            //shows information for the chosen alignment
            Console.WriteLine(info.AlignmentInfo(alignment));
            Console.ReadLine();

            //Create your Ability
            chosen = Ask("Your Abilities will now be rolled: \n(0) Strength \n(1) Dexterity \n(2) Constitution \n(3) Intelligence \n(4) Wisdom \n(5) Charisma \n");
            for (int i = 0; i < 6; i++)
            {
                stats[i] = random.CreateAbilityScores();
                Console.WriteLine(stats[i] + " = " + Enum.GetName(typeof(AbilityScore), i));
            }
            Console.ReadLine();


            // create your name
            int raceValue = default(int);
            int maleName = default(int);
            int femaleName = default(int);
            int lastName = default(int);

            String lName = default(String);
            String fName = default(String);

            loopThrough = true;

            raceValue = random.RaceValue();

            // character name menu list ===================================================
            loopThrough = true;
            while (loopThrough == true)
            {
                Console.WriteLine("\nChoose your {0}, {1}:", Enum.GetName(typeof(Selection), 5), race);

                // switch statement to choose first name
                switch (raceValue)
                {
                    case 0:
                        // Dwarf
                        maleName = 0;
                        femaleName = 1;
                        for (int i = 0; i < 6; i++)
                        {
                            Console.WriteLine("{0, -20} {1, 4:N1}", ("(" + i + ") " + Name[0, i]), ("(" + (i + 6) + ") " + Name[1, i]));
                        }
                        break;
                    case 1:
                        // Elf
                        maleName = 3;
                        femaleName = 4;
                        for (int i = 0; i < 6; i++)
                        {
                            Console.WriteLine("{0, -20} {1, 4:N1}", ("(" + i + ") " + Name[3, i]), ("(" + (i + 6) + ") " + Name[4, i]));
                        }
                        break;
                    case 2:
                        // Halfling
                        maleName = 6;
                        femaleName = 7;
                        for (int i = 0; i < 6; i++)
                        {
                            Console.WriteLine("{0, -20} {1, 4:N1}", ("(" + i + ") " + Name[6, i]), ("(" + (i + 6) + ") " + Name[7, i]));
                        }
                        break;
                    case 3:
                        // Human
                        maleName = 9;
                        femaleName = 10;
                        for (int i = 0; i < 6; i++)
                        {
                            Console.WriteLine("{0, -20} {1, 4:N1}", ("(" + i + ") " + Name[9, i]), ("(" + (i + 6) + ") " + Name[10, i]));
                        }
                        break;
                }

                Console.WriteLine("\n\n X - Random");

                // loop for manual or random or wrong input
                while (loopThrough == true)
                {
                    // random selection input
                    chosen = Console.ReadLine();
                    if (chosen.Equals("x") || chosen.Equals("X"))
                    {
                        fName = random.CreatefName(maleName, femaleName);
                        loopThrough = false;
                    }
                    // manual selection input
                    else if (chosen.Equals("0") ||
                             chosen.Equals("1") ||
                             chosen.Equals("2") ||
                             chosen.Equals("3") ||
                             chosen.Equals("4") ||
                             chosen.Equals("5") ||
                             chosen.Equals("6") ||
                             chosen.Equals("7") ||
                             chosen.Equals("8") ||
                             chosen.Equals("9") ||
                             chosen.Equals("10") ||
                             chosen.Equals("11"))
                    {
                        fName = manual.CreatefName(chosen, maleName, femaleName);
                        loopThrough = false;
                    }
                    else
                    {
                        loopThrough = true;
                    }




                }

                Console.WriteLine("\nChoose your Last Name, {0}:", fName);

                // switch statement to choose last name
                switch (raceValue)
                {
                    case 0:
                        // Dwarf
                        lastName = 2;
                        for (int i = 0; i < 6; i++)
                        {
                            Console.WriteLine("(" + i + ") " + Name[2, i]);
                        }

                        break;
                    case 1:
                        // Elf
                        lastName = 5;
                        for (int i = 0; i < 6; i++)
                        {
                            Console.WriteLine("(" + i + ") " + Name[5, i]);
                        }

                        break;
                    case 2:
                        // Halfling
                        lastName = 8;
                        for (int i = 0; i < 6; i++)
                        {
                            Console.WriteLine("(" + i + ") " + Name[8, i]);
                        }

                        break;
                    case 3:
                        // Human
                        lastName = 11;
                        for (int i = 0; i < 6; i++)
                        {
                            Console.WriteLine("(" + i + ") " + Name[11, i]);
                        }

                        break;
                }

                Console.WriteLine("\n\n X - Random");

                // loop for manual or random or wrong input
                loopThrough = true;
                while (loopThrough == true)
                {
                    // random selection input
                    chosen = Console.ReadLine();
                    if (chosen.Equals("x") || chosen.Equals("X"))
                    {

                        lName = random.CreatelName(lastName);
                        loopThrough = false;
                    }
                    // manual selection input
                    else if (chosen.Equals("0") ||
                             chosen.Equals("1") ||
                             chosen.Equals("2") ||
                             chosen.Equals("3") ||
                             chosen.Equals("4") ||
                             chosen.Equals("5"))
                    {
                        lName = manual.CreatelName(chosen, lastName);
                        loopThrough = false;
                    }
                    else
                    {
                        loopThrough = true;
                    }
                }

                name = fName + " " + lName;


            }
            // printing name for user review
            Console.WriteLine("Your name is {0}", name);
            Console.ReadKey();



            player = new Player(name, alignment, race, pClass, stats, speed);
            dnDSkills.ListSkills();
            player.AddSkill(dnDSkills.GetSkill());
            player.AddSkill(dnDSkills.GetSkill());

            spells.ListSpells();
            player.AddSpell(spells.GetSpell());
            player.AddSpell(spells.GetSpell());


            //Saving Player into a file
            if (!player.GetDetails().Equals(saveLoad.Load(playerSaves[playerChosen].Name).GetDetails()))
            {
                while (loop)
                {
                    chosen = Ask(player.GetDetails() + "\nWould you like to save? (y/n)\n ");
                    loop = chosen.Equals("y") || chosen.Equals("n") ? false : true;
                }
                loop = true;
            }
            if (chosen.Equals("y"))
            {
                saveLoad.Save(player);
            }
        }

        private void Load()
        {
            //Asks Player to make the choice between different save files
            string characterSelection = "Choose from 0 - " + (playerSaves.Length - 1);
            for (int i = 0; i < playerSaves.Length; i++)
            {
                characterSelection += "\n(" + i + ")" + saveLoad.Load(playerSaves[i].Name).GetName();
            }
            //Continue to do this code until the player chose to EXIT
            while (loop)
            {
                chosen = Ask(characterSelection + "\n" + "\n(e)xit\n");
                loop = int.TryParse(chosen, out playerChosen) && playerChosen >= 0 && playerChosen < playerSaves.Length ? false : true;
                if (chosen.Equals("e") || chosen.Equals("E"))
                {
                    chosen = "";
                    return;
                }
            }
            loop = true;
            player = saveLoad.Load(playerSaves[playerChosen].Name);

            //Enter code for updating skills and spells and stuff here. 
            edit = true;
                //Starts edit loop
            while (edit)
            {
                menuChoice = Ask(player.GetDetails() + FirstEditMenu());
                menuSwitch();
            }
            if (!player.GetDetails().Equals(saveLoad.Load(playerSaves[playerChosen].Name).GetDetails()))
            {
                while (loop)
                {
                    chosen = Ask(player.GetDetails() + "\nWould you like to save? (y/n)\n ");
                    loop = chosen.Equals("y") || chosen.Equals("n") ? false : true;
                }
                loop = true;
            }
            if (chosen.Equals("y"))
            {
                saveLoad.Save(player);
            }
        }

        private string Ask(string question)
        {
            Console.Clear();
            Console.Write(question);
            return Console.ReadLine();
        }



        //Methods i've added to make the stats and spells update menu - Raaid
        
            //Displays the list of spells and lets the player choose a new one
        public void ChooseAddSpell()
        {
            Console.Clear();
            Console.Write(player.GetDetails());
            spells.ListSpells();
            player.AddSpell(spells.GetSpell());
    
        }

            //Displays players current spells and allows player to remove one
        public void ChooseRemoveSpell()
        {   
            Console.Clear();
            Console.WriteLine(player.GetDetails());
            Console.WriteLine(player.GetCurrentSpells());
            Console.WriteLine("Please choose the number of the spell you would like to remove: ");
            player.RemoveSpell();
        }

            //Same as add spells but for skills
            public void ChooseAddSkill()
        {
            Console.Clear();
            Console.Write(player.GetDetails());
            dnDSkills.ListSkills();
            player.AddSkill(dnDSkills.GetSkill());
        }

            //Same as remove spells but for skills
        public void ChooseRemoveSkill()
        {
            Console.Clear();
            Console.WriteLine(player.GetDetails());
            Console.WriteLine(player.GetCurrentSkills());
            Console.WriteLine("Please choose the number of the skill you would like to remove: ");
            player.RemoveSkill();
        }

            //The first menu the player encounters when making edits to character
        public string FirstEditMenu()
        {
            return "\nWhat would you like to adjust:\n(sta)ts\n(spe)lls\n(ski)lls\n\n\n(e)xit\n";
        }

            //Takes input from first menu and directs player between stats, skills and spells
        public void menuSwitch()
        {
            Console.Clear();
            Console.Write(player.GetDetails());
            switch (menuChoice)
            {
                case "sta":
                    Console.WriteLine("\nWould you like to (i)ncrease or (d)ecrease a stat?\n");
                    Console.WriteLine("(e)xit\n");
                    statSwitch();
                    break;
                case "spe":
                    Console.WriteLine("\nWhat would you like to do:");
                    Console.WriteLine("(a)dd a spell");
                    Console.WriteLine("(r)emove a spell\n");
                    Console.WriteLine("(e)xit\n");
                    spellSwitch();
                    break;
                case "ski":
                    Console.WriteLine("\nWhat would you like to do:");
                    Console.WriteLine("(a)dd a skill");
                    Console.WriteLine("(r)emove a skill\n");
                    Console.WriteLine("(e)xit\n");
                    skillSwitch();
                    break;
                case "e":
                    exitEditLoop();
                    break;
                case "E":
                    exitEditLoop();
                    break;
            }
        }

            //Takes input from user to determine to add or remove a spell
        public void spellSwitch()
        {
            spellChoice = Console.ReadLine();
            switch (spellChoice)
            {
                case "a":
                case "A":
                    ChooseAddSpell();
                    break;
                case "r":
                case "R":
                    ChooseRemoveSpell();
                    break;
                case "e":
                case "E":
                    exitEditLoop();
                    break;
            }
        }

            //Same as spellSwitch(); but for skills
        public void skillSwitch()
        {
            spellChoice = Console.ReadLine();
            switch (spellChoice)
            {
                case "a":
                case "A":
                    ChooseAddSkill();
                    break;
                case "r":
                case "R":
                    ChooseRemoveSkill();
                    break;
                case "e":
                case "E":
                    exitEditLoop();
                    break;
            }
        }

            //Same concept as spellSwitch and skillSwitch but
            //increase and decrease instead of add/remove
        public void statSwitch()
        {
            statChoice = Console.ReadLine();
            switch (statChoice)
            {
                case "i":
                case "I":
                    incStat();
                    break;
                case "d":
                case "D":
                    decStat();
                    break;
                case "e":
                case "E":
                    exitEditLoop();
                    break;
            }
        }

            //Decreases players stat of choice
        private void decStat()
        {
            statChoice = Ask(player.GetDetails() + "\nType out which stat you would like to decrease (STR/DEX/CON/INT/WIS/CHA):\n");
            Console.WriteLine("\nHow much would you like to decrease the stat by");
            multiply = Int32.Parse(Console.ReadLine());
            for (int i = 0; i < multiply; i++)
            {
                player.DecreaseStat(statChoice);
            }
        }

            //Increases players stat of choice
        private void incStat()
        {
            statChoice = Ask(player.GetDetails() + "\nType out which stat you would like to increase (STR/DEX/CON/INT/WIS/CHA):\n");
            Console.WriteLine("\nHow much would you like to increase the stat by");
            multiply = Int32.Parse(Console.ReadLine());
            for (int i = 0; i < multiply; i++)
            {
                player.IncreaseStat(statChoice);
            }
        }

            //Breaks player out of edit loop
        private void exitEditLoop()
        {
            edit = false;
            return;
        }
    }
}
