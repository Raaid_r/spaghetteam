﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TonyMapCreator : MonoBehaviour {
    
    public GameObject mapTilePFB;
    public int width, height;

	void Start () 
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                GameObject tile = Instantiate(mapTilePFB, new Vector3(x, y), Quaternion.identity);
                tile.transform.SetParent(transform);
                tile.GetComponent<TonyMapTileData>().SetPosition(x, y);
            } 
        }
	}
}
