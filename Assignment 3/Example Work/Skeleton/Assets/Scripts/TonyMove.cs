﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TonyMove : MonoBehaviour {

    public GameObject player;
    public float speed;
    public GameObject plane;
    SpriteRenderer sr;
    bool isSprinting = false;
    public float energy = 100f;
    public int energyDepletion;
    public int energyRegenerate;
    public int sprintSpeed;
    bool isInvis;
    public float invisEnergy = 100f;
    public int invisEnergyDepletion;
    public int invisEnergyRegenerate;

	private void Start()
	{
        sr = gameObject.GetComponent<SpriteRenderer>();
	}

    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            if (player.transform.position.y < plane.transform.localScale.z * 5 - (sr.sprite.bounds.size.y * 0.5f))
            {
                if (isSprinting == true && energy > 0f)
                {
                    player.transform.position += Vector3.up * Time.deltaTime * speed * sprintSpeed;
                }
                    player.transform.position += Vector3.up * Time.deltaTime * speed;
            }
            //"Vector3.up" is the same as "Vector3 up = new Vector3(0, 1, 0)";
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            if (player.transform.position.y > plane.transform.localScale.z * -5 + (sr.sprite.bounds.size.y * 0.5f))
            {
                if (isSprinting == true && energy > 0f)
                {
                    player.transform.position += Vector3.down * Time.deltaTime * speed * sprintSpeed;
                }
                player.transform.position += Vector3.down * Time.deltaTime * speed;
            }
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (player.transform.position.x > plane.transform.localScale.x * -5 + (sr.sprite.bounds.size.y * 0.5f))
            {
                if (isSprinting == true && energy > 0f)
                {
                    player.transform.position += Vector3.left * Time.deltaTime * speed * 2;
                    energy -= Time.deltaTime * 100;
                }
                player.transform.position += Vector3.left * Time.deltaTime * speed;
            }
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            if (player.transform.position.x < plane.transform.localScale.x * 5 - (sr.sprite.bounds.size.y * 0.5f))
            {
                if (isSprinting == true && energy > 0f)
                {
                    player.transform.position += Vector3.right * Time.deltaTime * speed * 2;
                    energy -= Time.deltaTime * 100;
                }
                player.transform.position += Vector3.right * Time.deltaTime * speed;
            }
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            isSprinting = true;
            if (energy > 0)
            {
                energy -= Time.deltaTime * energyDepletion;
            }
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            isSprinting = false;
        }

        if (energy < 100 && isSprinting == false)
        {
            energy += Time.deltaTime * energyRegenerate;
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            isInvis = true;
        }
        if (Input.GetKeyUp(KeyCode.Z))
        {
            isInvis = false;
        }

        if (isInvis == true)
        {
            sr.color = new Color(1, 1, 1, 0.25f);
            if (invisEnergy > 0)
            {
                invisEnergy -= Time.deltaTime * invisEnergyDepletion;
            }
            else
            {
                isInvis = false;
            }
        }
        if (isInvis == false)
        {
            sr.color = new Color(1, 1, 1, 1);
            if (invisEnergy < 100)
            {
                invisEnergy += Time.deltaTime * invisEnergyRegenerate;
            }
        }
    }


}
