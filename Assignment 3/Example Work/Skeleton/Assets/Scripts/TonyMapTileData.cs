﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType { FOOD, GOLD };

public class TonyMapTileData : MonoBehaviour {
    
    bool visited, containsItem;
    int x, y;
    public ItemType itemType;

    //Init
	
    public void SetPosition(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public void SetContainsItem()
    {
        containsItem = true;
        itemType = ItemType.FOOD;
    }

    //Runtime
    public void SetVisited()
    {
        visited = true;
    }
}
