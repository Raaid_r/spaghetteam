﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Adder : MonoBehaviour {
    int a;
    int b;
    int value;
	// Use this for initialization
	void Start () {
        a = Random.Range(0, 100);
        b = Random.Range(0, 100);
        value = a + b;
        Debug.Log(a + " + " + b + " = " + value);
	}
	
	// Update is called once per frame
	void Update () {
	}
}
