﻿/*
HeroGroup
Manages the player's group of heroes for the map scene.

Copyright 2014 John M. Quick.

*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HeroGroup : MonoBehaviour {

	//the member sprites
	public List<Sprite> memberSprites; 
	
	//index of current member selected in group
	private int _currentMember; 
	
	//init
	void Start() {
		
		//init List
		memberSprites = new List<Sprite>();
		
		//add sprite of current player to List
		memberSprites.Add(gameObject.GetComponent<SpriteRenderer>().sprite);
		
		//start at first member
		_currentMember = 0;

	} //end function

	//update
	void Update() {

		//check for e key press
		if (Input.GetKeyDown(KeyCode.Space)) {

			//toggle member
			ToggleMember();

		} //end if

	} //end function
	
	//toggle between group members
	private void ToggleMember() {

		//Debug.Log("[HeroGroup] Key pressed; change group member");
		
		//increment current index
		_currentMember++;
		
		//verify that index is within bounds and reset if necessary
		if (_currentMember > memberSprites.Count - 1) {
			
			_currentMember = 0; //reset
			
		} //end if
		
		//update the renderer based on the current index
		gameObject.GetComponent<SpriteRenderer>().sprite = memberSprites[_currentMember];
				
	} //end function

} //end class