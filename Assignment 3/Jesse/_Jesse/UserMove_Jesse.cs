﻿/*
UserMove
Manages the movement of a player-controlled object.

Copyright 2014 John M. Quick.
*/

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UserMove : MonoBehaviour {

    //properties
	//the speed at which to move the object
    private float _speed; 
    // float to alter player speed based on tile collision -----------
    private float _speedBuff;
	
	//the updated direction
    private Vector2 _newDir;

    // UI for debuff/buff
    private float countdown;
    private GameObject SpeedUp;
    private GameObject SpeedDown;
    public Text countdownUI;
    private bool isBuffed;
    private bool isFast;
    private bool isSlow;
    private bool takeDmg;


    //awake
    void Awake()
    {

        //prevent player from being destroyed when application switches scenes
        DontDestroyOnLoad(this);
        countdown = 0f;
        SpeedUp = GameObject.Find("SpeedUp");
        SpeedUp.SetActive(false);
        SpeedDown = GameObject.Find("SpeedDown");
        SpeedDown.SetActive(false);
        countdownUI = GameObject.Find("countdownUI").GetComponent<Text>();
        isBuffed = false;


    } //end function
	
	//init
	void Start() {
		
		//properties
        _speed = 0.05f;
        _speedBuff = 1.0f;
        countdownUI.text = string.Format("{0:N3}", countdown);
	
	} //end function

	private void OnLevelWasLoaded(int level)
	{
        _speedBuff = 1f;
        SpeedUp.SetActive(false);
        SpeedDown.SetActive(false);
        countdown = 0f;
	}


	//update
	void Update()
    {

        //check user input
        CheckUserInput();

        //move object
        MoveObject();

        if (isBuffed == true)
        {
            takeDmg = false;
            countdown -= Time.deltaTime;
            if (countdown < 0f)
            {
                takeDmg = true;
                isBuffed = false;
                countdown = 0f;
            }
        }
        countdownUI.text = string.Format("{0:N3}", countdown);


    } //end function




    //check user input
    private void CheckUserInput() {

        /*
        Input Notes 
        GetKey returns true while key is held
        GetKeyDown returns true only when the key is initially pressed
        GetKeyUp returns true only when the key is initially released
        */

		//store the new movement direction based on user input
		int newDirX = 0; //default to no movement
		int newDirY = 0; //default to no movement

		//check for movement input
		//move up
		//if player holds up arrow
		if (Input.GetKey(KeyCode.UpArrow)) {
			
			//move up along the Y axis
			newDirY = 1; 

		} //end if

		//move down
		//if player holds down arrow
		if (Input.GetKey(KeyCode.DownArrow)) {
			
			//move down along the Y axis
			newDirY = -1; 

		} //end if

		//move left
		//if player holds left arrow
		if (Input.GetKey(KeyCode.LeftArrow)) {
			
			//move left along the X axis
			newDirX = -1; 

		} //end if

		//move right
		//if player holds right arrow
		if (Input.GetKey(KeyCode.RightArrow)) {
			
			//move right along the X axis
			newDirX = 1; 

		} //end if

		//update current direction attempted
		_newDir = new Vector2(newDirX, newDirY);

    } //end function

    //move the object to its new position
    private void MoveObject()
    {

        //retrieve the player's current position
        Vector3 currentPos = gameObject.transform.position;

        //change in movement based on user input and speed --------------------------
        float deltaMoveX = _newDir.x * _speed * _speedBuff; //the x-axis change
        float deltaMoveY = _newDir.y * _speed * _speedBuff; //the y-axis change
		
		//new coordinates based on movement alone
        float newX = currentPos.x + deltaMoveX;
        float newY = currentPos.y + deltaMoveY;

        //check x axis
		//left edge of player is past left side of screen
		if (newX - 0.5f * gameObject.GetComponent<SpriteRenderer>().bounds.size.x < -0.5f * Screen.width / 100.0f) {

			//stop player at edge
            newX = -0.5f * Screen.width / 100.0f + 0.5f * gameObject.GetComponent<SpriteRenderer>().bounds.size.x;

		} //end if
		
		//right edge of player is past right side of screen
		if (newX + 0.5f * gameObject.GetComponent<Renderer>().bounds.size.x > 0.5f * Screen.width / 100.0f) {
		
			//stop player at edge
            newX = 0.5f * Screen.width / 100.0f - 0.5f * gameObject.GetComponent<SpriteRenderer>().bounds.size.x;

		} //end if
		
		//check y axis
		//top edge of player is past top side of screen
        if (newY + 0.5f * gameObject.GetComponent<SpriteRenderer>().bounds.size.y > 0.5f * Screen.height / 100.0f) {

			//stop player at edge
            newY = 0.5f * Screen.height / 100.0f - 0.5f * gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

		} //end if

		//bottom edge of player is past bottom side of screen
        if (newY - 0.5f * gameObject.GetComponent<SpriteRenderer>().bounds.size.y < -0.5f * Screen.height / 100.0f) {
		
			//stop player at edge
            newY = -0.5f * Screen.height / 100.0f + 0.5f * gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

		} //end if

        //store the movement position based user input and speed
        Vector3 movePos = new Vector3(
            newX,
            newY,
            currentPos.z
            );
		
        //update object position
		gameObject.transform.position = movePos;

    } //end function

    //check collisions
    void OnTriggerEnter2D(Collider2D theCollider)
    {
        //disable collisions
        theCollider.enabled = false;

		//retrieve the tag for the collider's game object
        string tag = theCollider.gameObject.tag;
		
        //retrieve collectable inventory
        //used for collectable and drake collisions
        CollectableInventory collectInventory = GameObject.FindWithTag("Inventory").GetComponent<CollectableInventory>();

        //check the tag
        switch (tag) {

            //stairs down
            case "StairsDown":

                //check heroes saved
                //if all heroes saved
                if (StateManager.Instance.heroesSaved >= 3) {

                    //reset game
                    StateManager.Instance.ResetGame();

                } //end if

                //if heroes remain
                else {

                    //continue to next dungeon level
                    StateManager.Instance.SwitchSceneTo("Dungeon");

                } //end else

                break;
				
			//collectable 
            case "Collectable":

                //inventory has space remaining
                if (collectInventory.inventory.Count < collectInventory.maxObjects) {

                    //add collectable to inventory
                    collectInventory.AddItem(theCollider.gameObject);

                    //Debug.Log("[UserMove] Collectable found!");

                } //end else if
                                
                //otherwise, reenable collisions
                //item may be collected again later
                else {

                    //enable collisions
                    theCollider.enabled = true;

                } //end else

                break;

            //hero 
            case "Hero":

                //increment counter in state manager
                StateManager.Instance.heroesSaved++;

                //retrieve hero sprite from collision
                Sprite heroSprite = theCollider.GetComponent<SpriteRenderer>().sprite;

                //add to the hero group
                gameObject.GetComponent<HeroGroup>().memberSprites.Add(heroSprite);

                //destroy
                Destroy(theCollider.gameObject);

                //Debug.Log("[UserMove] Hero added to party!");

                break;

            //drake
            case "Drake":

                //check inventory
                //if inventory is empty
                if (collectInventory.inventory.Count <= 0) {

                    //reset game
                    StateManager.Instance.ResetGame();

                } //end if

                //inventory has items remaining
                else if (collectInventory.inventory.Count > 0) {

                    //remove collectable from inventory
                    collectInventory.RemoveItem();

                    //destroy
                    Destroy(theCollider.gameObject);

                } //end else if

                //Debug.Log("[UserMove] Collided with drake!");

                break;

                //Slow -----------------------------------
            case "Slow":

                //check to see what buff/debuff is currently active
                // if its a slow, let it tick, and if tick is done, deal dmg, if other, cancel it and apply slow
                if (isSlow == true)
                {
                    if (takeDmg == true)
                    {
                        if (collectInventory.inventory.Count <= 0)
                        {

                            //reset game
                            StateManager.Instance.ResetGame();

                        } //end if

                        //inventory has items remaining
                        else if (collectInventory.inventory.Count > 0)
                        {

                            //remove collectable from inventory
                            collectInventory.RemoveItem();
                        }
                    }
                        else
                        {
                            
                        }
                    takeDmg = false;
                }
                else
                {
                    StopAllCoroutines();
                    isSlow = true;
                    isFast = false;
                    SpeedUp.SetActive(false);
                    _speedBuff = 0.5f;
                    StartCoroutine(Waitfor(3, false));

                }
                theCollider.enabled = true;
                break;

                //Speed -----------------------------------
            case "Speed":

                //check to see what buff/debuff is currently active
                if (isFast == true)
                {

                }
                else
                {
                    StopAllCoroutines();
                    isFast = true;
                    isSlow = false;
                    SpeedDown.SetActive(false);
                    _speedBuff = 1.5f;
                    StartCoroutine(Waitfor(3, true));
                }
                theCollider.enabled = true;
                break;

            //default
            default:
                Debug.Log("[UserMove] Collision game object tag not recognized");
                break;

        } //end switch


    } //end function

    // test timer ---------------

    IEnumerator Waitfor(float duration, bool isFaster)
    {
        countdown = 3f;
        isBuffed = true;
        if (isFaster ==  true)
        {
            SpeedUp.SetActive(true);

        }
        else
        {
            SpeedDown.SetActive(true);

        }

        Debug.Log("Start time is: " + Time.time);
        yield return new WaitForSeconds(duration);
        Debug.Log("End time is: " + Time.time);
        _speedBuff = 1.0f;

        if (isFaster == true)
        {
            SpeedUp.SetActive(false);

        }
        else
        {
            SpeedDown.SetActive(false);

        }
        isSlow = false;
        isFast = false;
    }

} //end class