﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour {

	//number of tiles to spawn
	public int numTiles;

	//tile prefab defined in Unity inspector
	public GameObject tilePrefab;

	//clone a given tile prefab a specified number of times
	public GameObject[] CloneTiles(GameObject thePrefab, int theNumClones) {

		//store clones in array
		GameObject[] cloneTiles = new GameObject[theNumClones];

		//populate cloned tiles array
		for (int i = 0; i < theNumClones; i++) {

			//clone prefab tile
			GameObject cloneTile = (GameObject)Instantiate(thePrefab);

			//add the tile to the map game object in Unity scene
			//set parent
			cloneTile.transform.parent = gameObject.transform;

			//add to clone array
			cloneTiles[i] = cloneTile;

		} //end for

		//return clone array
		return cloneTiles;

	} //end function

	//spawn tiles at randomly generated map positions
	public void SpawnTilesAtRandPos(GameObject[] theTiles, List<Vector2> theOpenPos, int theTileSize, int thePixelsToUnits) {

		//loop through tiles
		foreach (GameObject aTile in theTiles) {

			//select the next open map position
			float randCol = theOpenPos[0].x; 
			float randRow = theOpenPos[0].y;

			//remove the used position
			theOpenPos.RemoveAt(0);

			//calculate the position in world units
			//x position
			float xPos = (randCol * theTileSize - Screen.width / 2 + theTileSize / 2) / thePixelsToUnits;

			//y position
			float yPos = (Screen.height / 2 - randRow * theTileSize - theTileSize / 2) / thePixelsToUnits;

			//z position
			float zPos = gameObject.transform.position.z;

			//set tile position
			aTile.transform.position = new Vector3(xPos, yPos, zPos);

			//Debug.Log("[MapSpawn] Tile spawned at: (" + randX + ", " + randY + ")");

		} //end foreach

	} //end function

} //end class