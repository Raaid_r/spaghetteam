﻿/*
CollectableInventory
Manages an inventory of collectable objects.

Copyright 2014 John M. Quick.
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CollectableInventory : MonoBehaviour {

	//the stored inventory of collected objects
	public List<GameObject> inventory;


    //maximum number of objects in inventory; defined in Unity Inspector
    public int maxObjects;

    //scale for the collectables; defined in Unity Inspector
    public float objectScale;

    //awake
    void Awake() {

        //prevent player from being destroyed when application switches scenes
        DontDestroyOnLoad(this);

    } //end function

	//init
	void Start() {

		//properties
		inventory = new List<GameObject>();
	
	} //end function

	//add an object to the inventory
	public void AddItem(GameObject theItem) {

        //check whether space remains in inventory
        if (inventory.Count < maxObjects) {

            //add the item to the inventory
            inventory.Add(theItem);
			
			//position the item in the inventory
			//creates a row of inventory items along the bottom of the screen

            //set the item's scale
            theItem.transform.localScale = new Vector3(objectScale, objectScale, 1.0f);
			
			//retrieve the current position
			Vector3 pos = theItem.transform.position;
			
			//retrieve the size
			Vector3 size = theItem.GetComponent<SpriteRenderer>().bounds.size;
			
			//set the x position inside the inventory
			//align the latest item at the end of the preceding items
			float xPos = (-0.5f * Screen.width / 100.0f) + 0.5f * size.x + (inventory.Count - 1) * size.x;
			
			//set the y position at the bottom of the screen
			float yPos = (-0.5f * Screen.height / 100.0f) + 0.5f * size.y; 

            //update position values
			pos.x = xPos;
			pos.y = yPos;
		
			//set position
			theItem.transform.position = pos;

			//add item to inventory GameObject in scene
			theItem.transform.parent = gameObject.transform;

        } //end if

	} //end function

	//remove the most recent object from the inventory
	public void RemoveItem() {

		//only remove if at least one item exists
		if (inventory.Count > 0) {
		
			//store item
			GameObject lastItem = inventory[inventory.Count - 1];
			
			//remove the last item in the inventory
			inventory.RemoveAt(inventory.Count - 1);

			//destroy item
			Destroy(lastItem);	

		} //end if

	} //end function

} //end function