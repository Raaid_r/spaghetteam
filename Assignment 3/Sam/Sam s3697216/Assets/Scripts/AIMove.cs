﻿/*
AIMove
/*
AIMove
Manages the movement of a non-player-controlled object. 
The AI will find the farthest edge of the screen and 
move towards it, then return back to the origin.

Copyright 2014 John M. Quick.
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum MoveType
{
    Line,
    Random,
    Chase,
}

public class AIMove : MonoBehaviour
{

    //speed boundaries
    public float minSpeed;
    public float maxSpeed;
    public MoveType moveType = MoveType.Random;

    //speed object is moving
    private float _speed;

    //origin position of the object
    private Vector3 _originPos;

    //destination position of the object
    private Vector3 _destPos;

    //init
    void Start()
    {

        //generate random speed
        _speed = Random.Range(minSpeed, maxSpeed);

        //set origin
        _originPos = gameObject.transform.position;

        //calculate destination
        CalcDestPos();

    } //end function

    //update
    void Update()
    {

        //move object
        MoveObjectTo(gameObject, _destPos);

        //check destination
        CheckPosFor(gameObject, _destPos);

    } //end function

    private void CalcDestPos()
    {
        if (moveType == MoveType.Line)
        {
            _destPos = FindEdgeFor(gameObject);
        }
        else if (moveType == MoveType.Random)
        {
            _destPos = FindRandomPos();
        }
        else if (moveType == MoveType.Chase)
        {
            _destPos = FindChaseTargetPos();
        }
        else
        {
            _destPos = _originPos;
        }
    }

    private Vector3 FindRandomPos()
    {
        var newX = Random.Range(-0.5f, 0.5f) * Screen.width / 100.0f;
        var newY = Random.Range(-0.5f, 0.5f) * Screen.height / 100.0f;
        var newZ = transform.position.z;
        return new Vector3(newX, newY, newZ);
    }

    //calculate farthest screen edge given object
    private Vector3 FindEdgeFor(GameObject theObject)
    {

        //get object properties
        Vector3 currentPos = theObject.transform.position;
        float objWidth = theObject.GetComponent<SpriteRenderer>().bounds.size.x;
        float objHeight = theObject.GetComponent<SpriteRenderer>().bounds.size.y;

        //store distance to edges of screen
        float distUp = Mathf.Abs(0.5f * Screen.height / 100.0f - 0.5f * objHeight - currentPos.y);
        float distDown = Mathf.Abs(-0.5f * Screen.height / 100.0f + 0.5f * objHeight - currentPos.y);
        float distLeft = Mathf.Abs(-0.5f * Screen.width / 100.0f + 0.5f * objWidth - currentPos.x);
        float distRight = Mathf.Abs(0.5f * Screen.width / 100.0f - 0.5f * objWidth - currentPos.x);

        //find the maximum distance
        float maxDist = Mathf.Max(distUp, distDown, distLeft, distRight);

        //store position variables
        float edgeX = currentPos.x;
        float edgeY = currentPos.y;
        float edgeZ = currentPos.z;

        //update position based on direction
        //up
        if (maxDist == distUp)
        {

            //update y
            edgeY += distUp;

        } //end if

        //down
        else if (maxDist == distDown)
        {

            //update y
            edgeY -= distDown;

        } //end if

        //left
        else if (maxDist == distLeft)
        {

            //update x
            edgeX -= distLeft;

        } //end if

        //right
        else if (maxDist == distRight)
        {

            //update x
            edgeX += distRight;

        } //end if

        //create destination
        Vector3 edgePos = new Vector3(edgeX, edgeY, edgeZ);

        //return
        return edgePos;

    } //end function

    //move the object towards destination
    private void MoveObjectTo(GameObject theObject, Vector3 theDestPos)
    {

        //retrieve current world position
        Vector3 currentPos = theObject.transform.position;

        //store new coordinates
        float newX = currentPos.x;
        float newY = currentPos.y;
        float newZ = currentPos.z;

        //update movement based on speed and direction
        //up
        if (currentPos.y < theDestPos.y - _speed)
        {

            //update y
            newY += _speed;

        } //end if

        //down
        else if (currentPos.y > theDestPos.y + _speed)
        {

            //update y
            newY -= _speed;

        } //end if

        //at destination
        else
        {

            //update y
            newY = theDestPos.y;

        } //end if

        //left
        if (currentPos.x > theDestPos.x + _speed)
        {

            //update x
            newX -= _speed;

        } //end if

        //right
        else if (currentPos.x < theDestPos.x - _speed)
        {

            //update x
            newX += _speed;

        } //end if

        //at destination
        else
        {

            //update x
            newX = theDestPos.x;

        } //end if

        //store the movement position based user input and speed
        Vector3 movePos = new Vector3(newX, newY, newZ);

        //update object position
        theObject.transform.position = movePos;

    } //end function

    private Vector3 FindChaseTargetPos()
    {
        var target = GameObject.Find("Player");
        if (target != null)
            return target.transform.position;
        else
            return _destPos;
    }

    //check whether object reached destination
    private void CheckPosFor(GameObject theObject, Vector3 theDestPos)
    {

        //if object has reached destination
        if (moveType == MoveType.Line)
        {
            if (theObject.transform.position == theDestPos)
            {

                //update destination position
                _destPos = _originPos;

                //update origin position
                _originPos = gameObject.transform.position;

            } //end if
        }
        else if (moveType == MoveType.Random)
        {
            if (theObject.transform.position == theDestPos)
            {

                //update destination position
                _destPos = FindRandomPos();
            }
        }
        else if (moveType == MoveType.Chase)
        {
            _destPos = FindChaseTargetPos();
        }


    } //end function

} //end class