﻿/using UnityEngine;
using System.Collections;

public class UserMoveRaaid : MonoBehaviour
{


    //Movement variables
    public float _speed;
    private Vector2 _newDir;

    //Sprite variables
    private SpriteRenderer outputSprite;
    public Sprite[] SpritesLeft;
    public Sprite[] SpritesRight;
    public Sprite[] SpritesUp;
    public Sprite[] SpritesDown;
    private float secondsToWait;
    private float FPS = 1f;
    private int currentFrame;
    private bool stopped = false;

    //Input checkers while in unity
    private bool up;
    private bool down;
    private bool left;
    private bool right;

    //Sprinting variables
    private bool isSprinting = false;
    public float energy = 100f;
    private int energyDepletion;
    private int energyRegeneration;

    public float invincibilityLength;
    private float invincibilityCounter;



    void Awake()
    {
        DontDestroyOnLoad(this);
        _speed = 0.05f;

        //Gets SpriteRenderer
        outputSprite = this.GetComponent<SpriteRenderer>();

        //currentFrame
        currentFrame = 0;

        //Sets speed of animation   30 FPS means 30 "cycles" per second - higher number, faster animation
        if (FPS > 0)
        {
            secondsToWait = 1 / FPS;
        }
        else
        {
            secondsToWait = 1 / 5f;
        }

        //Sprinting regen variable
        energyDepletion = 100;
        energyRegeneration = 100;

    }

    void Start()
    {
        //Code to see which input is being pressed
        left = false;
        right = false;
        down = false;
        up = false;
    }

    void Update()
    {
        CheckUserInput();
        MoveObject();
        PlayerSprint();

        //invincibility
		if (invincibilityCounter > 0) {
			invincibilityCounter -= Time.deltaTime;

		}

    }

    private void CheckUserInput()
    {

        int newDirX = 0;
        int newDirY = 0;

        //Added method getters for animation
        if (Input.GetKey(KeyCode.UpArrow))
        {
            newDirY = 1;
            up = true;
        }


        if (Input.GetKey(KeyCode.DownArrow))
        {
            newDirY = -1;
            down = true;
        }



        if (Input.GetKey(KeyCode.LeftArrow))
        {
            newDirX = -1;
            left = true;
        }



        if (Input.GetKey(KeyCode.RightArrow))
        {
            newDirX = 1;
            right = true;
        }


        //Added sprinting when left shift is held
        if (Input.GetKeyDown(KeyCode.LeftShift) && energy > 0)
        {
            isSprinting = true;
            _speed = _speed * 2;
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            isSprinting = false;
            _speed = 0.05f;
        }

        //update current direction attempted
        _newDir = new Vector2(newDirX, newDirY);

        if (newDirX == 0 && newDirY == 1) //Up
        {
            CancelInvoke("AnimateUp");
            CancelInvoke("AnimateDown");
            CancelInvoke("AnimateLeft");
            CancelInvoke("AnimateRight");
            AniUpCommand();
        }
        if (newDirX == 0 && newDirY == -1) //Down
        {
            CancelInvoke("AnimateUp");
            CancelInvoke("AnimateDown");
            CancelInvoke("AnimateLeft");
            CancelInvoke("AnimateRight");
            AniDownCommand();
        }
        if (newDirX == -1 && newDirY == 0) //Left
        {
            CancelInvoke("AnimateUp");
            CancelInvoke("AnimateDown");
            CancelInvoke("AnimateLeft");
            CancelInvoke("AnimateRight");
            AniLeftCommand();
        }
        if (newDirX == 1 && newDirY == 0) //Right
        {
            CancelInvoke("AnimateUp");
            CancelInvoke("AnimateDown");
            CancelInvoke("AnimateLeft");
            CancelInvoke("AnimateRight");
            AniRightCommand();
        }
        if (newDirX == -1 && newDirY == 1) //Up + left
        {
            CancelInvoke("AnimateUp");
            CancelInvoke("AnimateDown");
            CancelInvoke("AnimateLeft");
            CancelInvoke("AnimateRight");
            AniUpCommand();
        }
        if (newDirX == 1 && newDirY == 1) //Up + Right
        {
            CancelInvoke("AnimateUp");
            CancelInvoke("AnimateDown");
            CancelInvoke("AnimateLeft");
            CancelInvoke("AnimateRight");
            AniUpCommand();
        }
        if (newDirX == -1 && newDirY == -1) //Down + Left
        {
            CancelInvoke("AnimateUp");
            CancelInvoke("AnimateDown");
            CancelInvoke("AnimateLeft");
            CancelInvoke("AnimateRight");
            AniDownCommand();
        }
        if (newDirX == 1 && newDirY == -1) //Down + Right
        {
            CancelInvoke("AnimateUp");
            CancelInvoke("AnimateDown");
            CancelInvoke("AnimateLeft");
            CancelInvoke("AnimateRight");
            AniDownCommand();
        }

        if (newDirX == 0 && newDirY == 0) //Idle
        {
            CancelInvoke("AnimateUp");
            CancelInvoke("AnimateDown");
            CancelInvoke("AnimateLeft");
            CancelInvoke("AnimateRight");
            stopped = true;
            outputSprite.sprite = SpritesDown[1];
        }
    }


    //move the object to its new position
    private void MoveObject()
    {


        Vector3 currentPos = gameObject.transform.position;

        //change in movement based on user input and speed
        float deltaMoveX = _newDir.x * _speed; //the x-axis change
        float deltaMoveY = _newDir.y * _speed; //the y-axis change

        //new coordinates based on movement alone
        float newX = currentPos.x + deltaMoveX;
        float newY = currentPos.y + deltaMoveY;

        //check x axis
        //left edge of player is past left side of screen
        if (newX - 0.5f * gameObject.GetComponent<SpriteRenderer>().bounds.size.x < -0.5f * Screen.width / 100.0f)
        {

            //stop player at edge
            newX = -0.5f * Screen.width / 100.0f + 0.5f * gameObject.GetComponent<SpriteRenderer>().bounds.size.x;

        } //end if

        //right edge of player is past right side of screen
        if (newX + 0.5f * gameObject.GetComponent<Renderer>().bounds.size.x > 0.5f * Screen.width / 100.0f)
        {

            //stop player at edge
            newX = 0.5f * Screen.width / 100.0f - 0.5f * gameObject.GetComponent<SpriteRenderer>().bounds.size.x;

        } //end if

        //check y axis
        //top edge of player is past top side of screen
        if (newY + 0.5f * gameObject.GetComponent<SpriteRenderer>().bounds.size.y > 0.5f * Screen.height / 100.0f)
        {

            //stop player at edge
            newY = 0.5f * Screen.height / 100.0f - 0.5f * gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        } //end if

        //bottom edge of player is past bottom side of screen
        if (newY - 0.5f * gameObject.GetComponent<SpriteRenderer>().bounds.size.y < -0.5f * Screen.height / 100.0f)
        {

            //stop player at edge
            newY = -0.5f * Screen.height / 100.0f + 0.5f * gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        } //end if

        //store the movement position based user input and speed
        Vector3 movePos = new Vector3(
            newX,
            newY,
            currentPos.z
            );

        //update object position
        gameObject.transform.position = movePos;

    } //end function



    //check collisions
    void OnTriggerEnter2D(Collider2D theCollider)
    {

        //disable collisions
        theCollider.enabled = false;

        //retrieve the tag for the collider's game object
        string tag = theCollider.gameObject.tag;

        //retrieve collectable inventory
        //used for collectable and drake collisions
        CollectableInventory collectInventory = GameObject.FindWithTag("Inventory").GetComponent<CollectableInventory>();

  
          //check the tag
        switch (tag)
        {

                case "StairsDown":

                //check heroes saved
                //if all heroes saved
                if (StateManager.Instance.heroesSaved >= 3)
                {

                    //reset game
                    StateManager.Instance.ResetGame();

                } //end if

                //if heroes remain
                else
                {

                    //continue to next dungeon level
                    StateManager.Instance.SwitchSceneTo("Dungeon");
                    CancelInvoke("AnimateUp");
                    CancelInvoke("AnimateDown");
                    CancelInvoke("AnimateLeft");
                    CancelInvoke("AnimateRight");
                    if (Input.GetKeyDown(KeyCode.LeftShift))
                    {
                        _speed = 0.1f;
                    }
                    else
                    {
                        _speed = 0.05f;
                    }

                } //end else

                break;

            //collectable 
            case "Collectable":

                //inventory has space remaining
                if (collectInventory.inventory.Count < collectInventory.maxObjects)
                {

                    //add collectable to inventory
                    collectInventory.AddItem(theCollider.gameObject);

                    //Debug.Log("[UserMove] Collectable found!");

                } //end else if

                //otherwise, reenable collisions
                //item may be collected again later
                else
                {

                    //enable collisions
                    theCollider.enabled = true;

                } //end else

                break;

            //hero 
            case "Hero":

                ////increment counter in state manager
                //StateManager.Instance.heroesSaved++;

                ////retrieve hero sprite from collision
                //Sprite heroSprite = theCollider.GetComponent<SpriteRenderer>().sprite;

                ////add to the hero group
                //gameObject.GetComponent<HeroGroup>().memberSprites.Add(heroSprite);

                ////destroy
                //Destroy(theCollider.gameObject);

                ////Debug.Log("[UserMove] Hero added to party!");

                //check inventory
                //if inventory is empty
                if (collectInventory.inventory.Count < 5)
                {

                    //reset game
                    StateManager.Instance.ResetGame();

                } //end if

                //inventory has items remaining
                else
                {
                    while (collectInventory.inventory.Count > 0)
                    {
                        //remove collectable from inventory
                        collectInventory.RemoveItem();
                    }

                    StateManager.Instance.heroesSaved++;

                    Sprite heroSprite = theCollider.GetComponent<SpriteRenderer>().sprite;

                    gameObject.GetComponent<HeroGroup>().memberSprites.Add(heroSprite);

                    Destroy(theCollider.gameObject);
                } //end else if

                //Debug.Log("[UserMove] Collided with drake!");
                break;

            //drake
            case "Drake":

                //check inventory
                //if inventory is empty
                if (collectInventory.inventory.Count <= 0)
                {

                    //reset game
                    StateManager.Instance.ResetGame();

                } //end if

                //inventory has items remaining
                else if (collectInventory.inventory.Count > 0)
                {

                    //remove collectable from inventory
                    collectInventory.RemoveItem();

                    //destroy
                    Destroy(theCollider.gameObject);

                } //end else if

                //Debug.Log("[UserMove] Collided with drake!");

                break;


		case "Invulnerables":
			
			invincibilityCounter = invincibilityLength;

			Destroy (theCollider.gameObject);


			break;


            //default
            default:
                Debug.Log("[UserMove] Collision game object tag not recognized");
                break;

        } //end switch

    } //end function
        //Sprite Animations
    public void AniLeftCommand(bool reset = false)
    {
        if (reset == true)
        {
            currentFrame = 0;
        }

        stopped = false;
        outputSprite.enabled = true;

        if (SpritesLeft.Length > 1)
        {
            AnimateLeft();
        }
        else if (SpritesLeft.Length > 0)
        {
            outputSprite.sprite = SpritesLeft[1];
        }
    }
    public virtual void AnimateLeft()
    {
        CancelInvoke("AnimateLeft");
        //changes sprite every "secondsToWait"and resets once at end of array
        if (currentFrame >= SpritesLeft.Length)
        {
            currentFrame = 0;
        }
        outputSprite.sprite = SpritesLeft[currentFrame];
        if (!stopped)
        {
            currentFrame++;
            Invoke("AnimateLeft", secondsToWait);
        }


    }

    public void AniRightCommand(bool reset = false)
    {
        if (reset == true)
        {
            currentFrame = 0;
        }
        stopped = false;
        outputSprite.enabled = true;
        if (SpritesRight.Length > 1)
        {
            AnimateRight();
        }
        else if (SpritesRight.Length > 0)
        {
            outputSprite.sprite = SpritesRight[1];
        }
    }
    public virtual void AnimateRight()
    {
        CancelInvoke("AnimateRight");
        if (currentFrame >= SpritesRight.Length)
        {
            currentFrame = 0;
        }
        outputSprite.sprite = SpritesRight[currentFrame];

        if (!stopped)
        {
            currentFrame++;
            Invoke("AnimateRight", secondsToWait);
        }
    }

    public void AniUpCommand(bool reset = false)
    {
        if (reset == true)
        {
            currentFrame = 0;
        }

        stopped = false;
        outputSprite.enabled = true;

        if (SpritesUp.Length > 1)
        {
            AnimateUp();
        }

        else if (SpritesUp.Length > 0)
        {
            outputSprite.sprite = SpritesUp[1];
        }
    }
    public virtual void AnimateUp()
    {
        CancelInvoke("AnimateUp");
        if (currentFrame >= SpritesUp.Length)
        {
            currentFrame = 0;
        }
        outputSprite.sprite = SpritesUp[currentFrame];

        if (!stopped)
        {
            currentFrame++;
            Invoke("AnimateUp", secondsToWait);
        }
    }

    public void AniDownCommand(bool reset = false)
    {
        if (reset == true)
        {
            currentFrame = 0;
        }
        stopped = false;
        outputSprite.enabled = true;

        if (SpritesDown.Length > 1)
        {
            AnimateDown();
        }
        else if (SpritesDown.Length > 0)
        {
            outputSprite.sprite = SpritesDown[1];
        }
    }
    public virtual void AnimateDown()
    {
        CancelInvoke("AnimateDown");
        if (currentFrame >= SpritesDown.Length)
        {
            currentFrame = 0;
        }
        outputSprite.sprite = SpritesDown[currentFrame];

        if (!stopped)
        {
            currentFrame++;
            Invoke("AnimateDown", secondsToWait);
        }
    }

    //Sprint method  based on prac class work
    public void PlayerSprint()
    {
        if (energy < 100 && isSprinting == false)
        {
            energy += Time.deltaTime * energyRegeneration;
        }

        if (energy > 0 && isSprinting == true)
        {
            energy -= Time.deltaTime * energyDepletion;
        }

        if (energy <= 0)
        {
            _speed = 0.05f;
        }

    }

} //end class
} //end class