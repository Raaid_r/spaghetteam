﻿/*
AIMove
Manages the movement of a non-player-controlled object. 
The AI will find the farthest edge of the screen and 
move towards it, then return back to the origin.

Copyright 2014 John M. Quick.
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Movement
{
    Idle, Horizontal, Vertical, ZigZag, Follow, Random
}

public class AIMove : MonoBehaviour {

    public float minSpeed, maxSpeed;
    public float fps;
    public Movement movement;
    public Sprite[] spritesIdle, spritesUp, spritesDown, spritesLeft, spritesRight;

    private float _speed;
    private float _time, _startTime;
    private Vector3 _originPos, _destPos;
    private Vector3 _playerNewPos, _playerOriginPos;
    private int _currentSprite;
    private float _length;

    void Start() 
    {
        _speed = Random.Range(minSpeed, maxSpeed);
        _startTime = Time.time;
        _currentSprite = 0;
        _originPos = gameObject.transform.position;
        _playerOriginPos = GameObject.FindWithTag("Player").transform.position;

        //generate random movement pattern if chosen to do so
        if (movement == Movement.Random)
        {
            int i = Random.Range(0, 5);
            movement = i == (int)Movement.Idle ? Movement.Idle : movement;
            movement = i == (int)Movement.Horizontal ? Movement.Horizontal : movement;
            movement = i == (int)Movement.Vertical ? Movement.Vertical : movement;
            movement = i == (int)Movement.ZigZag ? Movement.ZigZag : movement;
            movement = i == (int)Movement.Follow ? Movement.Follow : movement;
        }
        //set movement
        switch(movement)
        {
            
            case Movement.Horizontal:
            case Movement.ZigZag:
                _destPos = FindEdgeFor(gameObject, true, false);
                break;
            case Movement.Vertical:
                _destPos = FindEdgeFor(gameObject, false, true);
                break;
        }

        //if furthest is down, set it so they go down when they reach the end of the zig zag
        if (FindEdgeFor(gameObject, false, true).y < 0)
        {
            _length = -0.64f;
        }
        else
        {
            _length = 0.64f;
        }

    }

    void Update() {

        //check if game object exists in the program
        if (GameObject.FindWithTag("Player") != null)
        {
            //set the new players position to the current player position
            _playerNewPos = GameObject.FindWithTag("Player").transform.position;
        }

        //check if player has changed position
        if (!_playerNewPos.Equals(_playerOriginPos))
        {
            switch (movement)
            {
                case Movement.Vertical:
                case Movement.Horizontal:
                    MoveObjectTo(gameObject, _destPos);
                    //check if gameobject has reached it's destination
                    if (gameObject.transform.position == _destPos)
                    {

                        //update destination position
                        _destPos = _originPos;

                        //update origin position
                        _originPos = gameObject.transform.position;

                    }
                    Animate(fps + _speed * 100, false);
                    break;
                case Movement.Follow:

                    //set destination to be player's position
                    _destPos = GameObject.Find("Player").transform.position;

                    //move object
                    MoveObjectTo(gameObject, _destPos);
                    Animate(fps + _speed * 100, false);
                    break;
                case Movement.ZigZag:

                    //move object
                    MoveObjectTo(gameObject, _destPos);

                    //if object has reach the destination
                    if (gameObject.transform.position == _destPos)
                    {
                        //if object goes out of screen, go opposite direction
                        if (((_originPos.y + _length) < -0.5f * (Screen.height / 100.0f - gameObject.GetComponent<SpriteRenderer>().bounds.size.y) ) ||
                            ((_originPos.y + _length) > 0.5f * (Screen.height / 100.0f - gameObject.GetComponent<SpriteRenderer>().bounds.size.y)) )
                        {
                            _length *= -1;
                        } //end if;
                        //go up/down 1 block
                        _destPos = new Vector3(_originPos.x, _originPos.y + _length, _originPos.z);
                        //update origin position
                        _originPos = gameObject.transform.position;
                        _originPos.y += _length;
                        Animate(fps + _speed * 100, false);
                    } //end if;
                    Animate(fps + _speed * 100, false);
                    break;
                case Movement.Idle:
                    Animate(fps, true);
                    break;
            }
            _playerOriginPos = _playerNewPos;
        }
        else
        {
            Animate(fps, true);
        }

    } //end function

    private void Animate(float fps, bool idle)
    {
        float run = _destPos.x - gameObject.transform.position.x;
        float rise = _destPos.y - gameObject.transform.position.y;
        float gradient;
        if (run == 0)
        {
            gradient = Mathf.Abs(rise);
        }
        else
        {
            gradient = Mathf.Abs(rise / run);
        }
        switch (gameObject.tag)
        {
            case "Drake":
                if (run >= 0)
                {
                    gameObject.GetComponent<SpriteRenderer>().flipX = false;
                }
                else
                {
                    gameObject.GetComponent<SpriteRenderer>().flipX = true;
                }
                _time = Time.time - _startTime;
                if (_time > 1 / fps)
                {
                    _currentSprite++;
                    _startTime = Time.time;
                }
                _currentSprite = _currentSprite < spritesIdle.Length ? _currentSprite : 0;
                gameObject.GetComponent<SpriteRenderer>().sprite = spritesIdle[_currentSprite];
                break;
            case "Hero":
                if (idle)
                {
                    _time = Time.time - _startTime;
                    if (_time > 1 / fps)
                    {
                        _currentSprite++;
                        _startTime = Time.time;
                    }
                    _currentSprite = _currentSprite < spritesIdle.Length ? _currentSprite : 0;
                    gameObject.GetComponent<SpriteRenderer>().sprite = spritesIdle[_currentSprite];
                }
                else
                {
                    if (run >= 0 && gradient < 1)
                    {
                        //right
                        _time = Time.time - _startTime;
                        if (_time > 1 / fps)
                        {
                            _currentSprite++;
                            _startTime = Time.time;
                        }
                        _currentSprite = _currentSprite < spritesRight.Length ? _currentSprite : 0;
                        gameObject.GetComponent<SpriteRenderer>().sprite = spritesRight[_currentSprite];

                    }
                    else if (run < 0 && gradient < 1)
                    {
                        //left
                        _time = Time.time - _startTime;
                        if (_time > 1 / fps)
                        {
                            _currentSprite++;
                            _startTime = Time.time;
                        }
                        _currentSprite = _currentSprite < spritesLeft.Length ? _currentSprite : 0;
                        gameObject.GetComponent<SpriteRenderer>().sprite = spritesLeft[_currentSprite];
                    }
                    else if (rise >= 0 && gradient >= 1)
                    {
                        //up
                        _time = Time.time - _startTime;
                        if (_time > 1 / fps)
                        {
                            _currentSprite++;
                            _startTime = Time.time;
                        }
                        _currentSprite = _currentSprite < spritesUp.Length ? _currentSprite : 0;
                        gameObject.GetComponent<SpriteRenderer>().sprite = spritesUp[_currentSprite];
                    }
                    else if (rise < 0 && gradient >= 1)
                    {
                        //down
                        _time = Time.time - _startTime;
                        if (_time > 1 / fps)
                        {
                            _currentSprite++;
                            _startTime = Time.time;
                        }
                        _currentSprite = _currentSprite < spritesDown.Length ? _currentSprite : 0;
                        gameObject.GetComponent<SpriteRenderer>().sprite = spritesDown[_currentSprite];
                    }
                }
                break;
        }
    }

    //calculate farthest screen edge given object
    private Vector3 FindEdgeFor(GameObject theObject, bool horizontal, bool vertical) {

        //get object properties
        Vector3 currentPos = theObject.transform.position;
        float objWidth = theObject.GetComponent<SpriteRenderer>().bounds.size.x;
        float objHeight = theObject.GetComponent<SpriteRenderer>().bounds.size.y;

        //store distance to edges of screen
        float distUp = Mathf.Abs(0.5f * Screen.height / 100.0f - 0.5f * objHeight - currentPos.y);
        float distDown = Mathf.Abs(-0.5f * Screen.height / 100.0f + 0.5f * objHeight - currentPos.y);
        float distLeft = Mathf.Abs(-0.5f * Screen.width / 100.0f + 0.5f * objWidth - currentPos.x);
        float distRight = Mathf.Abs(0.5f * Screen.width / 100.0f - 0.5f * objWidth - currentPos.x);
        float maxDist;
        if (horizontal && vertical)
        {
            //find the maximum distance
            maxDist = Mathf.Max(distUp, distDown, distLeft, distRight);
        }
        else if (horizontal)
        {
            maxDist = Mathf.Max(distLeft, distRight);
        }
        else if (vertical)
        {
            maxDist = Mathf.Max(distUp, distDown);
        }
        else
        {
            maxDist = 0;
        }
        //store position variables
        float edgeX = currentPos.x;
        float edgeY = currentPos.y;
        float edgeZ = currentPos.z;

        //update position based on direction
        //up
        if (maxDist == distUp) {

            //update y
            edgeY += distUp;

        } //end if

        //down
        else if (maxDist == distDown) {

            //update y
            edgeY -= distDown;

        } //end if

        //left
        else if (maxDist == distLeft) {

            //update x
            edgeX -= distLeft;

        } //end if

        //right
        else if (maxDist == distRight) {

            //update x
            edgeX += distRight;

        } //end if

        //create destination
        Vector3 edgePos = new Vector3(edgeX, edgeY, edgeZ);

        //return
        return edgePos;

    } //end function

    //move the object towards destination
    private void MoveObjectTo(GameObject theObject, Vector3 theDestPos) {

        //retrieve current world position
        Vector3 currentPos = theObject.transform.position;

        //store new coordinates
        float newX = currentPos.x;
        float newY = currentPos.y;
        float newZ = currentPos.z;

        //update movement based on speed and direction
        //up
        if (currentPos.y < theDestPos.y - _speed) {

            //update y
            newY += _speed;

        } //end if

        //down
        else if (currentPos.y > theDestPos.y + _speed) {

            //update y
            newY -= _speed;

        } //end if

        //at destination
        else {

            //update y
            newY = theDestPos.y;

        } //end if

        //left
        if (currentPos.x > theDestPos.x + _speed) {

            //update x
            newX -= _speed;

        } //end if

        //right
        else if (currentPos.x < theDestPos.x - _speed) {

            //update x
            newX += _speed;

        } //end if

        //at destination
        else {

            //update x
            newX = theDestPos.x;

        } //end if

        //store the movement position based user input and speed
        Vector3 movePos = new Vector3(newX, newY, newZ);

        //update object position
        theObject.transform.position = movePos;

    } //end function

    //check whether object reached destination
    private void CheckPosFor(GameObject theObject, Vector3 theDestPos) {

        //if object has reached destination
        if (theObject.transform.position == theDestPos) {

            //update destination position
            _destPos = _originPos;

            //update origin position
            _originPos = gameObject.transform.position;

        } //end if

    } //end function
} //end class