﻿/*
MapGenerator
Manages the generation of maps.

Copyright 2014 John M. Quick.
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapGenerator : MonoBehaviour {

    //default size of tiles, in pixels
    public int tileSize;

    //pixels to Unity world units conversion
    public int pixelsToUnits;

    //position for player when map is loaded
    public Vector2 playerPos;

    //available positions in middleground layer
    private List<Vector2> _middleOpenPos;

    //init
    void Start() {

        //update camera size
		StateManager.Instance.UpdateCamSize(Screen.height, pixelsToUnits);
	
		//determine map size based on screen/tile size
        int numCol = Screen.width / tileSize;
        int numRow = Screen.height / tileSize;

        //populate middleground with open positions
        _middleOpenPos = CreatePos(numCol, numRow);

        //randomize order of open positions
        RandSortPos(_middleOpenPos);

        //create background map
        CreateBgMap();
	
		//spawn objects on middleground map
        //position player
        PositionPlayerAt(playerPos);
		
		//stairs
		SpawnObjectsWithTag("Stairs");

        //SlowTile
        SpawnObjectsWithTag("Slow");

        //SpeedTile
        SpawnObjectsWithTag("Speed");

        //heroes
        SpawnHeroes(StateManager.Instance.heroesSaved, StateManager.Instance.levelsCompleted);

        //collectables
        SpawnObjectsWithTag("Collectables");

        int spawnCheck1 = Random.Range (0, 2);
        if (spawnCheck1 == 0) {
            SpawnObjectsWithTag ("Invulnerables");
        } 

        //drakes
        SpawnDrakes(StateManager.Instance.levelsCompleted);

    } //end function

    //populate the open map positions
	private List<Vector2> CreatePos(int theNumCol, int theNumRow) {
	
		//create collection to store open positions
		List<Vector2> allPos = new List<Vector2>();
		
        //populate open positions
        //iterate through columns
        for (int col = 0; col < theNumCol; col++) {

            //iterate through rows
            for (int row = 0; row < theNumRow; row++) {

                //store position
                Vector2 pos = new Vector2(col, row);

                //add position
                allPos.Add(pos);

            } //end inner for

        } //end outer for
		
		//return open positions
		return allPos;
	
	} //end function

    //randomize order of open positions
	//implements the Sattolo sorting method
	private void RandSortPos(List<Vector2> thePositions) {
		
		//start counter at last item in collection
        int indexCounter = thePositions.Count;
		
		//loop through items
        while (indexCounter > 1) {

            //decrement counter
            indexCounter--;

            //store a copy of the original value
            Vector2 original = new Vector2(thePositions[indexCounter].x, thePositions[indexCounter].y);

            //calculate random index value
            int randIndex = Random.Range(0, indexCounter);

            //swap the original value for the random 
            thePositions[indexCounter] = thePositions[randIndex];

            //swap the random value for the original
            thePositions[randIndex] = original;

        } //end while
		
	} //end function

    //create background map
    private void CreateBgMap() {

        //get map object from scene
        RandomMap bgMap = GameObject.FindWithTag("BgMap").GetComponent<RandomMap>();

        //determine map properties based on screen size
        int numCol = Screen.width / tileSize;
        int numRow = Screen.height / tileSize;

        //generate map array
        int[,] middleMap = bgMap.CreateMapWithSize(numCol, numRow);

        //display tiles
        bgMap.DisplayMap(middleMap, tileSize, pixelsToUnits);

    } //end function
	
	//position player
    private void PositionPlayerAt(Vector2 thePos) {

        //get player game object from scene
        GameObject player = GameObject.FindWithTag("Player");

        //calculate player position in world units
        //x position
        float xPos = 
            thePos.x * player.GetComponent<SpriteRenderer>().bounds.size.x - 
            0.5f * Screen.width / pixelsToUnits + 
            0.5f * player.GetComponent<SpriteRenderer>().bounds.size.x;
        
        //y position
        float yPos = 
            0.5f * Screen.height / pixelsToUnits -
			thePos.y * player.GetComponent<SpriteRenderer>().bounds.size.y -
            0.5f * player.GetComponent<SpriteRenderer>().bounds.size.y; 

        //z position
        float zPos = player.transform.position.z; 

        //store position
        Vector3 playerPos = new Vector3(xPos, yPos, zPos);

        //set position
        player.transform.position = playerPos;

        //exclude player pos
        //prevents immediate collision when loading scene
        _middleOpenPos.Remove(thePos);

    } //end function

    //spawn objects associated with a specific tag
    private void SpawnObjectsWithTag(string theTag) {

        //retrieve game object associated with tag
        GameObject parentObject = GameObject.FindWithTag(theTag);

        //verify that object exists in scene
        if (parentObject != null) {

            //retrieve spawn script
            MapSpawn spawnScript = parentObject.GetComponent<MapSpawn>();

            //if more tiles have been specified than are open
            if (spawnScript.numTiles > _middleOpenPos.Count) {

                //restrict to number of available tiles
                spawnScript.numTiles = _middleOpenPos.Count;

            } //end if

            //clone tiles
            GameObject[] spawnTiles = spawnScript.CloneTiles(spawnScript.tilePrefab, spawnScript.numTiles);

            //spawn tile at random position
            spawnScript.SpawnTilesAtRandPos(spawnTiles, _middleOpenPos, tileSize, pixelsToUnits);

        } //end if

    } //end function

    //spawn heroes
    private void SpawnHeroes(int theNumHeroesSaved, int theNumLevelsCompleted) {

        /* 
        A hero will be spawned every few levels at 
        random. By design, the heroes will appear 
        in this order: Dryad, Dwarf, Orc. Subsequent 
        heroes will not be spawned until the 
        previous ones have been saved.
        */

        //determine whether this level should have a hero spawned 
        //25% chance that the check value will = 0
        int spawnCheck = Random.Range(0, 4);

        //Debug.Log("[MapManager] Hero spawn check: " + spawnCheck);

        //the check value will equal zero every few levels
        //skip the first few levels
        if (spawnCheck == 0 && theNumLevelsCompleted > 3) {

            //check which heroes have been saved
            switch (theNumHeroesSaved) {

                //none
                case 0:

                    //spawn dryad
                    SpawnObjectsWithTag("Dryad");
                    break; 

                //dryad only
                case 1:

                    //spawn dwarf
                    SpawnObjectsWithTag("Dwarf");
                    break; 

                //dryad and dwarf
                case 2:

                    //spawn orc
                    SpawnObjectsWithTag("Orc");
                    break;

                //default
                default:
                    Debug.Log("[MapManager] No heroes left to spawn");
                    break;

            } //end switch            

        } //end if

    } //end function

    //spawn a specified number of drakes
    private void SpawnDrakes(int theNumDrakes) {

        //store all of the drake tags in an array
        string[] tags = {
                            "BlueDrake", 
                            "GreenDrake", 
                            "GreyDrake", 
                            "OrangeDrake", 
                            "PinkDrake"
                        };

        //set up counter to keep track of spawns
        int numSpawned = 0;

        //while there are still spawns remaining
        while (numSpawned < theNumDrakes) {

            //randomly select an object tag
            string randTag = tags[Random.Range(0, tags.Length)];

            //spawn the object with the associated tag
            SpawnObjectsWithTag(randTag);

            //increment counter
            numSpawned++;

        } //end while

    } //end function

} //end class