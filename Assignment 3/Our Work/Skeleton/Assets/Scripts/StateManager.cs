﻿/*
StateManager
Manages the application state by handling scene switching. Uses a singleton instance.

Copyright 2014 John M. Quick.
*/

using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class StateManager:MonoBehaviour {

	//singleton instance
    private static StateManager _Instance;

    //keep track of total number of levels completed
    public int levelsCompleted;

    //keep track of total number of heroes saved
    public int heroesSaved;

    //singleton accessor
    //access StateManager.Instance from other classes
    public static StateManager Instance {
        
		//create instance via getter
		get {
            
			//check for existing instance
            //if no instance
            if (_Instance == null) {

                //create game object
                GameObject StateManagerObj = new GameObject();
                StateManagerObj.name = "State Manager";

                //create instance
                _Instance = StateManagerObj.AddComponent<StateManager>();

            } //end if

            //return the instance
            return _Instance;

        } //end get

    } //end accessor

    //awake
    void Awake() {

        //prevent this script from being destroyed when application switches scenes
        DontDestroyOnLoad(this);

        //start with no levels completed
        levelsCompleted = 0;

        //start with no heroes saved
        heroesSaved = 0;

    } //end function

    //switch scene by name
    public void SwitchSceneTo(string theScene) {

        //check scene
		if (theScene == "Dungeon") {
			
			//increment levels completed
			levelsCompleted++;
			
		} //end if

        //load next scene

        //Application.LoadLevel(theScene);
        SceneManager.LoadScene(theScene);
    } //end function

    //update camera size based on screen resolution and pixels to units conversion
    public void UpdateCamSize(int theScreenHeight, int thePixelsToUnits) {

        //update camera orthographic size
        Camera.main.orthographicSize = 0.5f * theScreenHeight / thePixelsToUnits;

    } //end function

    //reset game
    public void ResetGame() {

        Debug.Log("[UserMove] Game over with " + StateManager.Instance.heroesSaved + " heroes saved and " + StateManager.Instance.levelsCompleted + " levels explored!");

        //reset levels completed
        levelsCompleted = 0;

        //reset heroes saved
        heroesSaved = 0;

        //destroy old player
        Destroy(GameObject.FindWithTag("Player"));

        //destroy old inventory
        Destroy(GameObject.FindWithTag("Inventory"));

        //load map scene
        SwitchSceneTo("Map");

    } //end function
    
} //end class